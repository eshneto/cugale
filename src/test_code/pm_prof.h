//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef PM_PROF_H
#define PM_PROF_H

#include <string>

#ifndef PM_PROFILING_ON

/**
 * Poor men's profiler. This class provides some tools for measuring execution times of code. If macro \p PM_PROFILING_ON is not defined, its functions perform no operation.
 */
struct pm_prof{
   /**
    * \brief Constructor.
    *
    * Create unnamed object.
    */
   pm_prof()
   {}
   /**
    * \brief Constructor.
    *
    * \param name Object's name.
    *
    * Create named object.
    */
   pm_prof( std::string name )
   {}

   /**
    * \brief Destructor
    *
    * This function takes car of outputting the acquired timing information.
    */
   ~pm_prof()
   {}

   /**
    * \brief Start code snippet timing.
    *
    * \param str Snippet's name.
    *
    * This function denotes the start of a code snippet execution.
    */
   void start( std::string str )
   {}
   /**
    * \brief Stop code snippet timing.
    *
    * This function denotes the end of a code snippet execution.
    */
   void stop()
   {}
};
#else

#include <chrono>
#include <iostream>
#include <list>
#include <tuple>

/**
 * Poor men's profiler. This class provides some tools for measuring execution times of code. If macro \p PM_PROFILING_ON is not defined, its functions perform no operation.
 */
struct pm_prof{
   std::chrono::steady_clock::time_point begin_; ///< Start time of code execution.
   std::chrono::steady_clock::time_point end_; ///< End time of code execution.
   std::chrono::steady_clock::time_point begin_global_; ///< Start time of pm_prof object creation.
   std::chrono::steady_clock::time_point end_global_; ///< Start time of pm_prof object destruction.

   std::string str_; ///< Name of executing code snippet.
   std::string name_; ///< Name of pm_prof object.
   std::list< std::tuple< std::string, std::chrono::microseconds > > durations_; ///< List of code snippet names and respective durations.
   std::chrono::microseconds total_global_; ///< Total duration of pm_prof object.
   std::chrono::microseconds total_; ///< Duration of code snippet execution.

   static unsigned indent_level_; ///< Indentation level.
   /**
    * Make indentation based on pm_prof::indent_level_.
    */
   static void make_indent()
   {
      for ( unsigned i = 0; i < indent_level_ - 1; ++i )
         std::cout << '\t';
   }

   /**
    * \brief Constructor.
    *
    * Create unnamed object.
    */
   pm_prof()
   : begin_global_( std::chrono::steady_clock::now() ), total_global_( 0 ), total_( 0 )
   {
      ++indent_level_;
      make_indent();
      std::cout << name_ << " started\n";
   }
   /**
    * \brief Constructor.
    *
    * \param name Object's name.
    *
    * Create named object.
    */
   pm_prof( std::string name )
   : begin_global_( std::chrono::steady_clock::now() ), name_( name ), total_global_( 0 ), total_( 0 )
   {
      ++indent_level_;
      make_indent();
      std::cout << name_ << " started\n";
   }

   /**
    * \brief Destructor
    *
    * This function takes car of outputting the acquired timing information.
    */
   ~pm_prof()
   {
      end_global_ = std::chrono::steady_clock::now();
      for ( auto it = durations_.begin(); it != durations_.end(); ++ it )
      {
         make_indent();
         std::cout << '\t' << std::get< 0 >( *it ) << ": " << std::get< 1 >( *it ).count() / 1e6 << "s\n";
      }
      make_indent();
      total_global_ = std::chrono::duration_cast< std::chrono::microseconds >( end_global_ - begin_global_ );
      std::cout << name_ << " elapsed: " << total_global_.count() / 1e6 << 's'
                                         << " (" << total_.count() / 1e6 << "s)\n";
      --indent_level_;
   }

   /**
    * \brief Start code snippet timing.
    *
    * \param str Snippet's name.
    *
    * This function denotes the start of a code snippet execution.
    */
   void start( std::string str )
   {
      str_ = str;
      begin_ = std::chrono::steady_clock::now();
   }
   /**
    * \brief Stop code snippet timing.
    *
    * This function denotes the end of a code snippet execution.
    */
   void stop()
   {
      end_ = std::chrono::steady_clock::now();
      durations_.push_back( std::make_tuple( str_,
                                             std::chrono::duration_cast< std::chrono::microseconds >( end_ - begin_ )
                                           )
                          );
      total_ += std::chrono::duration_cast< std::chrono::microseconds >( end_ - begin_ );
   }
};

unsigned pm_prof::indent_level_ = 0;

#endif // #ifndef PM_PROFILING_ON

#endif //#ifndef PM_PROF_H
