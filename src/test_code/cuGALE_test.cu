// The following 5 lines are required because of a bug preventing boost to be used with
// CUDA 9.0. Newer versions of boost seem not to have this issue.
#ifdef __CUDACC_VER__
#undef __CUDACC_VER__
#endif
#define __CUDACC_VER__ 90000
#include "cuGALE.h"
#include "gppfft.h"
#include "utils.h"

#include <complex>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

// #define DETERMINISTIC_RUN
#define KNEE_EXPERIMENT_SIZE

typedef float real;

template < class data_t >
char const * type_string()
{
   return "unknown type";
}
template <>
char const * type_string< double >()
{
   return "double";
}
template <>
char const * type_string< float >()
{
   return "single";
}

int main()
{
   size_t free_mem = utils::device_mem_check();

   std::cout << "GPU computations performed in " << type_string< real >() << " precision floating point arithmetics.\n";

#ifdef DETERMINISTIC_RUN
   std::time_t seed = 1543959507;
#else
   std::time_t seed = std::time( 0 );
#endif // #ifdef DETERMINISTIC_RUN
   std::srand( seed );
#ifndef KNEE_EXPERIMENT_SIZE
   int m = 10 + 1 * ( std::rand() % 391 );
   int n = 10 + 1 * ( std::rand() % 391 );
   // N_L has to be larger than twice the maximum between m and n.
   // Furthermore, to match the definitions used in the CPU implementation,
   // N_L has to be divisible by 4:
   int N_L = ( 2 + std::rand() / double( RAND_MAX ) ) * std::max( m, n );
   N_L = N_L + ( 4 - N_L % 4 );
   int N = ( 1 + std::rand() / double( RAND_MAX ) ) * std::max( m, n );
   // P has to be larger than N_L, the difference P - N_L must be larger than 4
   // and N_L - P must also be divisible by 4:
   int P = N_L + ( 1 + std::rand() % 10 ) * 4;
   // M must be even and larger than or equal to the maximum between m and n:
   int M = ( 1 + std::rand() / double( RAND_MAX ) ) * std::max( m, n );
   M = M + M % 2;
#else
   int m = 256;
   int n = 256;
   // N_L has to be larger than twice the maximum between m and n.
   // Furthermore, to match the definitions used in the CPU implementation,
   // N_L has to be divisible by 4:
   int N_L = 512 + 256;
   int N = 128;
   // P has to be larger than N_L, the difference P - N_L must be larger than 4
   // and N_L - P must also be divisible by 4:
   int P = N_L + ( 1 + 3 ) * 4;
   // M must be even and larger than or equal to the maximum between m and n:
   int M = 512;
#endif // #ifndef KNEE_EXPERIMENT_SIZE
   std::cout << "Seed: " << seed << std::endl;
   std::cout << "M: " << M << std::endl;
   std::cout << "N: " << N << std::endl;
   std::cout << "N_L: " << N_L << std::endl;
   std::cout << "P: " << P << std::endl;
   std::cout << "m: " << m << std::endl;
   std::cout << "n: " << n << std::endl;

   // Generate vector of small golden angles:
   std::vector< double > angles = dft::golden_angles< double >( N );

   double sigma = utils::pi< double >::value / M;

   // Batch size (number of images):
   int b = 64;

   // P is the total numbers of rays, then P = N_L + 4 * ( S + 1 ):
   int S = ( P - N_L ) / 4 - 1;
   std::cout << "S: " << S << std::endl;

   // Creation of objects for DFT computation over the GALFD:
   cuGALE< real > cugale_object( m, n, b, M, N, &(angles[ 0 ]), S, N_L, sigma, sigma );
   gppfft<> gale_object( m, n, &(angles[ 0 ]), N, M, S, 0.0, N_L, 0, 4, FFTW_ESTIMATE, 0, sigma, sigma );

   // Generate random input data:
   cuda_array< std::complex< real > > in_page( m * n );
   in_page.host_alloc();
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
         in_page.host_data_[ i * n + j ] = std::complex< real >( std::rand() / real( RAND_MAX ),
                                                                 std::rand() / real( RAND_MAX )
                                                               );

   // Allocate memory for input on device:
   cuda_array< std::complex< real > > in( m * n * b );
   in.device_alloc();
   // Copy input to device:
   for ( int i = 0; i < b; ++i )
   {
      in_page.device_data_ = in.device_data_ + m * n * i;
      in_page.host_device();
   }
   in_page.device_data_ = 0;
   utils::device_mem_check();

   // Copy input data to CPU memory:
   std::complex< double > * cpu_in = new std::complex< double >[ m * n ];
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
         cpu_in[ i * n + j ] = in_page.host_data_[ i * n + j ];

   // Allocate memory for output:
   cuda_array< std::complex< real > > out( M * N * b );
   out.device_alloc();
   utils::device_mem_check();
   std::complex< double > * cpu_out = new std::complex< double >[ M * P ];

   // Allocate intermediate memory:
   cuda_array< std::complex< real > > buffer( M * P * b );
   buffer.device_alloc();
   utils::device_mem_check();

   // Perform computations:
   double tmp_gpu = 0.0;
   double tmp_cpu = 0.0;

   utils::time::tic();
   cugale_object.forward( in.device_data_, out.device_data_, buffer.device_data_ );
   utils::cuda_sync_check();
   tmp_gpu += utils::time::toc();

   utils::time::tic();
   gale_object.direct( cpu_in, reinterpret_cast< fftw_complex * >( cpu_out ) );
   tmp_cpu += utils::time::toc();

   std::cout << "Forward - GPU (" << b << " imgs.): " << tmp_gpu << 's' << std::endl;
   std::cout << "Forward - CPU: " << tmp_cpu << 's' << std::endl;
   std::cout << "Forward speed up: " << tmp_cpu / tmp_gpu * b << std::endl;

   // Determine which image to verify:
   int i_b = std::rand() % b;

   // Copy from device to host:
   cuda_array< std::complex< real > > out_page( M * N );
   out_page.host_alloc();
   out_page.device_data_ = out.device_data_ + i_b * M * N;
   out_page.device_host();
   out_page.device_data_ = 0;

   utils::save_array( "cpu_out.bin", M, N, reinterpret_cast< std::complex< double > * >( cpu_out ) );
   utils::save_array( "gpu_out.bin", M, N, out_page.host_data_ );

   // Create input for the adjoint (it can fit in memory used for forward output):
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
         cpu_out[ i * N + j ] = out_page.host_data_[ i * N + j ] = std::complex< real >(
            std::rand() / real( RAND_MAX ),
            std::rand() / real( RAND_MAX )
         );
   // Copy input for the adjoint to the GPU:
   for ( int i = 0; i < b; ++i )
   {
      out_page.device_data_ = out.device_data_ + M * N * i;
      out_page.host_device();
   }
   out_page.device_data_ = 0;

   double tmp_adjcpu = 0.0;
   utils::time::tic();
   gale_object.adjoint( cpu_out, reinterpret_cast< fftw_complex * >( cpu_in ) );
   tmp_adjcpu += utils::time::toc();

   double tmp_adjgpu = 0.0;
   utils::time::tic();
   cugale_object.adjoint( out.device_data_, in.device_data_, buffer.device_data_ );
   tmp_adjgpu += utils::time::toc();

   // Determine which image to verify:
   i_b = std::rand() % b;

   // Copy from device to host:
   in_page.device_data_ = in.device_data_ + i_b * m * n;
   in_page.device_host();
   in_page.device_data_ = 0;

   std::cout << "Adjoint - GPU (" << b << " imgs.): " << tmp_adjgpu << 's' << std::endl;
   std::cout << "Adjoint - CPU: " << tmp_adjcpu << 's' << std::endl;
   std::cout << "Adjoint speed up: " << tmp_adjcpu / tmp_adjgpu * b << std::endl;

   utils::save_array( "cpu_adjout.bin", m, n, cpu_in );
   utils::save_array( "gpu_adjout.bin", m, n, in_page.host_data_ );

   return 0;
}
