#include "cuLINOGRAM.h"
#include "cuda_array.h"
#include "utils.h"
#include "ppfft.h"
#include <complex>
#include <fftw3.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

// #define DETERMINISTIC_RUN

typedef float real;

template < class data_t >
char const * type_string()
{
   return "unknown type";
}
template <>
char const * type_string< double >()
{
   return "double";
}
template <>
char const * type_string< float >()
{
   return "single";
}

int main()
{
   size_t free_mem = utils::device_mem_check();

   std::cout << "GPU computations performed in " << type_string< real >() << " precision floating point arithmetics.\n";

#ifdef DETERMINISTIC_RUN
   std::time_t seed = 1543959507;
#else
   std::time_t seed = std::time( 0 );
#endif
   std::srand( seed );
   int m = 10 + 1 * ( std::rand() % 391 );
   int n = 10 + 1 * ( std::rand() % 391 );
   // N has to be larger than twice the maximum between m and n.
   // Furthermore, to match the definitions used in the CPU implementation,
   // N has to be divisible by 4:
   int N = ( 2 + std::rand() / double( RAND_MAX ) ) * std::max( m, n );
   N = N + ( 4 - N % 4 );
   // P has to be larger than N, the difference N - P must be larger than 4
   // and N - P must also be divisible by 4:
   int P = N + ( 1 + std::rand() % 10 ) * 4;
   // M must be larger than or equal to the maximum between m and n:
   int M = ( 1 + std::rand() / double( RAND_MAX ) ) * std::max( m, n );
   M = M + M % 2;
// #endif

   std::cout << "Seed: " << seed << std::endl;
   std::cout << "M: " << M << std::endl;
   std::cout << "N: " << N << std::endl;
   std::cout << "P: " << P << std::endl;
   std::cout << "m: " << m << std::endl;
   std::cout << "n: " << n << std::endl;

//    real sigma = 0;
   real sigma = utils::pi< real >::value / M;

   int b = 10;

   // P is the total numbers of rays, then P = N + 4 * ( S + 1 ):
   int S = ( P - N ) / 4 - 1;
   std::cout << ( P - N ) / 4.0 - 1.0 << std::endl;

   // Create linogram objects:
   ppfft::ppfft ppfft_obj( m, n, M, N, 4 * ( S + 1 ), 4, FFTW_ESTIMATE, 8, sigma, sigma );
   cuLINOGRAM< real > culinogram_obj( m, n, M, N, S, b, sigma, sigma );

   // Allocate memory for input:
   cuda_array< std::complex< real > > in( m * n * b );
   cuda_array< std::complex< real > > in_page( m * n );
   in_page.host_alloc();
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
         in_page.host_data_[ i * n + j ] = std::complex< real >( std::rand() / real( RAND_MAX ),
                                                                 std::rand() / real( RAND_MAX )
                                                               );
   in.device_alloc();
   for ( int i = 0; i < b; ++i )
   {
      in_page.device_data_ = in.device_data_ + m * n * i;
      in_page.host_device();
   }
   in_page.device_data_ = 0;
   utils::device_mem_check();

   std::complex< double > * cpu_in = new std::complex< double >[ m * n ];
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
         cpu_in[ i * n + j ] = in_page.host_data_[ i * n + j ];

   // Allocate memory for output:
   cuda_array< std::complex< real > > out( M * P * b );
   out.device_alloc();
   utils::device_mem_check();
   std::complex< double > * cpu_out = new std::complex< double >[ M * P ];

   // Allocate intermediate memory:
   cuda_array< std::complex< real > > buffer( M * P * b );
   buffer.device_alloc();

   // Perform computations:
   double tmp_gpu = 0.0;
   double tmp_cpu = 0.0;
   utils::time::tic();
   culinogram_obj.forward( in.device_data_, out.device_data_, buffer.device_data_ );
   utils::cuda_sync_check();
   tmp_gpu += utils::time::toc();
   std::cout << "Forward - GPU (" << b << " imgs.): " << tmp_gpu << 's' << std::endl;

   utils::time::tic();
   ppfft_obj.direct( cpu_in, reinterpret_cast< fftw_complex * >( cpu_out ) );
   tmp_cpu += utils::time::toc();
   std::cout << "Forward - CPU: " << tmp_cpu << 's' << std::endl;

   // Determine which image to verify:
   int i_b = std::rand() % b;
//    int i_b = b / 2;

   // Copy from device to host:
   cuda_array< std::complex< real > > out_page( M * P );
   out_page.host_alloc();
   out_page.device_data_ = out.device_data_ + i_b * M * P;
   out_page.device_host();
   out_page.device_data_ = 0;

   // Compute differences:
   double sum = 0.0;
   double mx = 0.0;
   for ( int i = 0; i < M; ++i )
   {
      for ( int j = 0; j < P; ++j )
      {
         double diff = std::abs( cpu_out[ i * P + j ] - std::complex< double >( out_page.host_data_[ i * P + j ] ) );
         diff /= std::abs( cpu_out[ i * P + j ] );
         if ( diff > mx )
         {
            mx = diff;
         }
         sum += diff;
      }
   }

   // Adjoint input:
   cuda_array< std::complex< real > > adjin_page( M * P );
   adjin_page.host_alloc();
   for ( int i = 0; i < M; ++i )
   {
      for ( int j = 0; j < P; ++j )
         adjin_page.host_data_[ i * P + j ] = std::complex< real >( std::rand() / real( RAND_MAX ),
                                                                    std::rand() / real( RAND_MAX )
                                                                  );
   }

   // Compute y^*Ax:
   std::complex< real > acc{ 0.0, 0.0 };
   for ( int i = 0; i < M; ++i )
      for ( int j = 0; j < P; ++j )
         acc += std::conj( adjin_page.host_data_[ i * P + j ] ) * out_page.host_data_[ i * P + j ];

   // Copy adjoint input to device:
   // TODO: Verify if all slices are equal!
   for ( int i = 0; i < b; ++i )
   {
      adjin_page.device_data_ = out.device_data_ + i * M * P;
      adjin_page.host_device();
   }
   adjin_page.device_data_ = 0;

   // Compute A^*y:
   // TODO: See whether there is enough home for the output:
   utils::time::tic();
   culinogram_obj.adjoint( out.device_data_, in.device_data_, buffer.device_data_ );
   double tmp_ajd_gpu = utils::time::toc();
   std::cout << "Adjoint - GPU (" << b << " imgs.): " << tmp_ajd_gpu << 's' << std::endl;

   // Copy result to host:
   cuda_array< std::complex< real > > adjout_page( m * n );
   adjout_page.device_data_ = in.device_data_ + i_b * m * n;
   adjout_page.host_alloc();
   adjout_page.device_host();
   adjout_page.device_data_ = 0;

   // Compute x^*(A^*y):
   std::complex< real > acc_adj{ 0.0, 0.0 };
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
         acc_adj += std::conj( in_page.host_data_[ i * n + j ] ) * adjout_page.host_data_[ i * n + j ];

   std::cout << "Average relative error: " << sum / ( M * P ) << std::endl;
   std::cout << "Maximum relative error: " << mx << std::endl;
   std::cout << "Speed-up factor: " << tmp_cpu / tmp_gpu * b << std::endl;

   std::cout << '|' << acc / std::conj( acc_adj ) << "| = " << std::abs( acc / std::conj( acc_adj ) ) << std::endl;
   std::cout << acc << std::endl;
   std::cout << acc_adj << std::endl;

   // Compute differences in shift factors:
   sum = 0.0;
   mx = 0.0;

   cuda_array< std::complex< real > > sf( M * m );
   sf.host_alloc();
   sf.device_data_ = culinogram_obj.czt_obj_horizontal_.shift_factors_;
   sf.device_host();
   sf.device_data_ = 0;
   std::complex< double > * aux_cpu = reinterpret_cast< std::complex< double > * >( ppfft_obj.chirp_z_bh_obj_.factors_ );
   for ( int i = 0; i < M; ++i )
   {
      for ( int j = 0; j < m; ++j )
      {
         double diff = std::abs( aux_cpu[ i * m + j ] - std::complex< double >( sf.host_data_[ i * m + j ] ) );
         diff /= std::abs( aux_cpu[ i * m + j ] );
         if ( diff > mx )
         {
            mx = diff;
         }
         sum += diff;
      }
   }

   std::cout << sum / ( M * m ) << std::endl;

   utils::save_array( "lino_cpu.bin", M, P, cpu_out );
   utils::save_array( "lino_gpu.bin", M, P, out_page.host_data_ );

   return 0;
}
