#include "cuCZT.h"
#include "chirp_z.h"
#include "cuda_array.h"
#include "utils.h"
#include <complex>
#include <fftw3.h>
#include <iostream>
#include <cstdlib>
#include <ctime>

// #define DETERMINISTIC_RUN

typedef float real;
typedef cuCZT< real > cuczt_t;

template < class data_t >
char const * type_string()
{
   return "unknown type";
}
template <>
char const * type_string< double >()
{
   return "double";
}
template <>
char const * type_string< float >()
{
   return "single";
}

int main()
{
   size_t free_mem = utils::device_mem_check();

   std::cout << "GPU computations performed in " << type_string< real >() << " precision floating point arithmetics.\n";

#ifdef DETERMINISTIC_RUN
   std::time_t seed = 1541540189;
   const int E = 8;
   const int P = 512;
   const int m = 500;
   const int M = 512;
//    const int E = 32;
//    const int P = 512;
//    const int m = 220;
//    const int M = 480;
#else
   std::time_t seed = std::time( 0 );
   std::srand( seed );

   const int E = ( std::rand() % 10 ) + 1;
   const int P = std::pow( 2.0, ( std::rand() % 4 ) + 8 );
   const int m = ( P - E );//std::min( P / 4, ( std::rand() % ( P - E ) ) + 1 );
   const int M = ( std::rand() % 2048 ) + 1;
#endif

   std::cout << "Seed: " << seed << '\n';

   size_t sl_size = 2 * P * M * 2 * sizeof( real );
   const int b = 1.5 * free_mem / ( 12 * sl_size );

   real sigma = utils::pi< real >::value / M;
   int R_1 = M / 2 - 1;
   int R_2 = P / 2;

   std::cout << "m = " << m << '\t';
   std::cout << "M = " << M << '\t';
   std::cout << "P = " << P << '\n';
   std::cout << "E = " << E << '\t';
   std::cout << "b = " << b << '\t';
   std::cout << "sigma = " << sigma << '\n';
   std::cout << "R_1 = " << R_1 << '\t';
   std::cout << "R_2 = " << R_2 << '\n';

   std::cout << "Initializing CZT object (GPU): ";
   utils::time::tic();
   cuczt_t czt_obj( m,  // Input length of each CZT
                    M,  // Number of CZTs
                    P,  // Output length of each CZT
                    E,  // Extra terms
                    b,  // Number of images
                    R_1, R_2, sigma // Domain parameters
                  );
   utils::time::tocr();

   utils::device_mem_check();

   int row_length = 2 * P;

   // Setup memory for CZT on the CPU:
   fftw_complex * cpu_in     = new fftw_complex[ row_length * M ];
   fftw_complex * cpu_adjin  = new fftw_complex[ row_length * M ];
   fftw_complex * cpu_buffer = new fftw_complex[ row_length * M ];
   fftw_complex * cpu_out    = new fftw_complex[ row_length * M ];

   // Init CPU CZT object:
   std::cout << "Initializing CZT object (CPU): ";
   fftw_init_threads();
   fftw_plan_with_nthreads( 12 );
   utils::time::tic();
   chirp_z cpuczt_obj( P - E, E, M, R_1, R_2, cpu_in, cpu_adjin, cpu_buffer, cpu_out, 6, FFTW_ESTIMATE, sigma );
   utils::time::tocr();

   std::cout << "Device memory allocation: ";
   utils::time::tic();
   cuda_array< std::complex< real > > in( m * M * b );
   in.device_alloc();
   cuda_array< std::complex< real > > out( 2 * P * M * b );
   out.device_alloc();
   cuda_array< std::complex< real > > buffer( 2 * P * M * b );
   buffer.device_alloc();
   utils::time::tocr();

   utils::device_mem_check();

   std::cout << "Random input generation: ";
   utils::time::tic();
   // Auxiliary host memory:
   cuda_array< std::complex< real > > partial_in( M * m );
   partial_in.host_alloc();
   // Generate random array:
   for ( int i = 0; i < M; ++i )
      for ( int j = 0; j < m; ++j )
         partial_in.host_data_[ i * m + j ] = std::complex< real >( real( std::rand() ) / RAND_MAX,
                                                                    real( std::rand() ) / RAND_MAX
                                                                  );
   // Copy to device:
   int i_b = std::rand() % b;
   for ( int i = 0; i < b; ++i )
   {
      partial_in.device_data_ = in.device_data_ + i * m * M;
      partial_in.host_device();
   }
   // partial_in does not own this device data:
   partial_in.device_data_ = 0;
   utils::time::tocr();

   // Compute CZTs:
   std::cout << "Computing CZTs (GPU - " << b << ( ( b > 1 ) ? " images): " : " image): " );
   utils::time::tic();
   czt_obj.forward( in.device_data_, out.device_data_, buffer.device_data_ );
   utils::cuda_sync_check();
   double GPU_forward_time = utils::time::tocr();

   // Copy data from GPU:
   for ( int i = 0; i < M; ++i )
   {
      for ( int j = 0; j < m; ++j )
      {
         cpu_in[ j * M + i ][ 0 ] = std::real( partial_in.host_data_[ i * m + j ] );
         cpu_in[ j * M + i ][ 1 ] = std::imag( partial_in.host_data_[ i * m + j ] );
      }
      for ( int j = m; j < row_length; ++j )
      {
         cpu_in[ j * M + i ][ 0 ] = real( 0.0 );
         cpu_in[ j * M + i ][ 1 ] = real( 0.0 );
      }

   }

   // Compute CZTs on the CPU:
   std::cout << "Computing CZTs (CPU - 1 image): ";
   utils::time::tic();
   cpuczt_obj.direct();
   double CPU_forward_time = utils::time::tocr();
   std::cout << "Speed up ratio (forward): " << ( CPU_forward_time * b ) / GPU_forward_time << '\n';

   double val;
   double mean;
   double mean_mx = 0.0;
   std::cout << "Computing forward differences...\n";
   for ( int i_b = 0; i_b < b; ++i_b )
   {
      // Get one of the slices from device to host:
      out.n_ = row_length * M;
      std::complex< real > * tmp = out.device_data_;
      out.device_data_ = out.device_data_ + i_b * row_length * M;
      out.host_alloc();
      out.device_host();
      out.device_data_ = tmp;

      // Compute mean relative difference:
      mean = 0.0;
      for ( int i = 0; i < M; ++i )
         for ( int j = 0; j < P; ++j )
         {
            mean += std::abs( std::complex< double >( out.host_data_[ j + i * row_length ] ) - reinterpret_cast< std::complex< double > * >( cpu_out )[ j + i * row_length ] ) / std::abs( reinterpret_cast< std::complex< double > * >( cpu_out )[ j + i * row_length ] );
         }
      mean /= ( P * M );
      if ( mean_mx < mean )
         mean_mx = mean;
   }
   std::cout << "Done!\n";

   // Generate input to the adjoint:
   cuda_array< std::complex< real > > adjin( row_length * M );
   adjin.host_alloc();
   for ( int i = 0; i < M; ++i )
   {
      for ( int j = 0; j < P; ++j )
         adjin.host_data_[ i * row_length + j ] = std::complex< real >( real( std::rand() ) / RAND_MAX, real( std::rand() ) / RAND_MAX );
      for ( int j = P; j < row_length; ++j )
         adjin.host_data_[ i * row_length + j ] = std::complex< real >( 0.0 );
   }

   // Compute y^HAx:
   std::complex< real > acc( 0.0 );

   for ( int i = 0; i < M; ++i )
      for ( int j = 0; j < P; ++j )
         acc += std::conj( adjin.host_data_[ j + i * row_length ] ) * out.host_data_[ j + i * row_length ];

   // Copy to device:
   for ( int i = 0; i < b; ++i )
   {
      adjin.device_data_ = out.device_data_ + i * row_length * M;
      adjin.host_device();
   }
   // Device memory not owned by adjin:
   adjin.device_data_ = 0;

   std::cout << "Computing adjoint CZTs (GPU - " << b << ( ( b > 1 ) ? " images): " : " image): " );
   utils::time::tic();
   czt_obj.adjoint( out.device_data_, in.device_data_, buffer.device_data_ );
   utils::cuda_sync_check();
   double GPU_adjoint_time = utils::time::tocr();

   // Copy data to CPU CZT input:
   for ( int i = 0; i < M; ++i )
      for ( int j = 0; j < row_length; ++j )
      {
         cpu_adjin[ i * row_length + j ][ 0 ] = std::real( adjin.host_data_[ i * row_length + j ] );
         cpu_adjin[ i * row_length + j ][ 1 ] = std::imag( adjin.host_data_[ i * row_length + j ] );
      }

   // Compute adjoints on the CPU:
   std::cout << "Computing adjoint CZTs (CPU - 1 image): ";
   utils::time::tic();
   cpuczt_obj.adjoint();
   double CPU_adjoint_time = utils::time::tocr();
   std::cout << "Speed up ratio (adjoint): " << ( CPU_adjoint_time * b ) / GPU_adjoint_time << '\n';

   double mean_adj;
   double mean_adj_mx = 0.0;
   cuda_array< std::complex< real > > adj_out( M * m );
   adj_out.host_alloc();
   for ( int i_b = 0; i_b < b; ++ i_b )
   {
      // Copy one slice of the adjoint output to the host:
      adj_out.device_data_ = in.device_data_ + i_b * M * m;
      adj_out.device_host();
      adj_out.device_data_ = 0;

      // Compute adjoint error:
      mean_adj = 0.0;
      for ( int i = 0; i < M; ++i )
      {
         for ( int j = 0; j < m; ++j )
         {
            mean_adj += std::abs( std::complex< double >( adj_out.host_data_[ j + i * m ] ) - reinterpret_cast< std::complex< double > * >( cpu_out )[ j + i * row_length ] ) / std::abs( reinterpret_cast< std::complex< double > * >( cpu_out )[ j + i * row_length ] );
         }
      }
      mean_adj /= ( M * m );
      if ( mean_adj_mx < mean_adj )
         mean_adj_mx = mean_adj;
   }

   // Compute x^H(A^Hy):
   std::complex< real > acc_adj( 0.0 );

   for ( int i = 0; i < M; ++i )
      for ( int j = 0; j < m; ++j )
         acc_adj += std::conj( partial_in.host_data_[ j + i * m ] ) * adj_out.host_data_[ j + i * m ];

   std::cout << "y^HAx: " << acc << '\n';
   std::cout << "x^H(A^Hy): " << acc_adj << '\n';
   std::cout << "\"Conjugacy factor\": |" << acc / std::conj( acc_adj ) << "| = " << std::abs( acc / std::conj( acc_adj ) ) << '\n';

   std::cout << "\n\n" << ( CPU_forward_time * b ) / GPU_forward_time << '\t' << ( CPU_adjoint_time * b ) / GPU_adjoint_time << '\n';
   std::cout << mean_mx << '\t' << mean_adj_mx << '\n';

   return 0;
}

/*
 * export PROGRAM=cuCZT_test && nvcc -O3 -std=c++11 -ccbin g++-5 -dc -m64 -o $PROGRAM.o -c $PROGRAM.cu &&nvcc -ccbin g++-5 -m64 -o $PROGRAM $PROGRAM.o -lcufft_static -lculibos -lfftw3 -lfftw3_threads && optirun ./$PROGRAM
 */
