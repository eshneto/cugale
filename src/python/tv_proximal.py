import torch

def L( p, q ):

   # p and q should have m lines and 2 * n columns
   # output will have m lines and 2 * n columns
   # We assume that p[ :, -1, : ] == 0 and that q[ :, :, -1 ] = q[ :, :, -2 ] == 0

   x = p + q
   x[ :, 1 :, : ] -= p[ :, : -1, : ]
   x[ :, :, 2 : ] -= q[ :, :, : -2 ]

   return x

def L_transpose( x, p = None, q = None ):

   # Assume x has m lines and 2 * n columns
   # p and q should have m lines and 2 * n columns
   # We assume that p[ :, -1, : ] == 0 and that q[ :, :, -1 ] = q[ :, :, -2 ] == 0

   sz = x.size()

   if p is None:
      p = torch.empty( sz, dtype = torch.float32, device = 'cuda' )
      p[ :, -1, : ] = 0.0

   if q is None:
      q = torch.empty( sz, dtype = torch.float32, device = 'cuda' )
      q[ :, :, -2 : ] = 0.0

   p[ :, : -1, : ] = x[ :, : -1, : ] - x[ :, 1 :, : ]
   q[ :, :, : -2 ] = x[ :, :, : -2 ] - x[ :, :, 2 : ]

   return ( p, q )

def projection( p, q, r = None, s = None ):

   if r is None:
      sz = p.size()
      r = torch.empty( sz, dtype = torch.float32, device = 'cuda' )
      r[ :, -1, : ] = 0.0

   if s is None:
      sz = q.size()
      s = torch.empty( sz, dtype = torch.float32, device = 'cuda' )
      s[ :, :, -2 : ] = 0.0

   # Same assumptions on p and q than for L( p, q ).

   den = torch.sqrt( p[ :, :, : : 2 ] ** 2 + p[ :, :, 1 : : 2 ] ** 2 +
                     q[ :, :, : : 2 ] ** 2 + q[ :, :, 1 : : 2 ] ** 2
                   )
   den = torch.max( den, torch.tensor( ( 1.0, ), dtype = torch.float32, device = 'cuda' ) )

   r[ :, :, : : 2 ] = p[ :, :, : : 2 ] / den
   r[ :, :, 1 : : 2 ] = p[ :, :, 1 : : 2 ] / den

   s[ :, :, : : 2 ] = q[ :, :, : : 2 ] / den
   s[ :, :, 1 : : 2 ] = q[ :, :, 1 : : 2 ] / den

   return ( r, s )

def denoise( b, l, N = 30 ):

   sz = b.size()

   # Previous step has to be recorded:
   p_prev = torch.zeros( sz, dtype = torch.float32, device = 'cuda' )
   q_prev = torch.zeros( sz, dtype = torch.float32, device = 'cuda' )

   # Iterations start from zero:
   r = torch.zeros( sz, dtype = torch.float32, device = 'cuda' )
   s = torch.zeros( sz, dtype = torch.float32, device = 'cuda' )

   #  Initial t
   t = 1.0

   # Stepsize that ensures convergence:
   step = 1.0 / ( 8.0 * l )

   for i in range( N ):

      # Compute gradient of dual objective function:
      x = L( r, s )
      x *= -l
      x += b
      ( grad_r, grad_s ) = L_transpose( x )

      # Update current iteration towards gradient direction:
      grad_r *= step
      grad_s *= step
      r += grad_r
      s += grad_s

      # Project into feasible set (proximal operator of the indicator function):
      ( p, q ) = projection( r, s )

      # t update
      t_prev = t
      t = ( 1.0 + ( 1 + 4.0 * ( t ** 2 ) ) ** 0.5 ) / 2

      # Momentum update for iterations:
      alpha = ( t_prev - 1 ) / t
      r = p + alpha * ( p - p_prev )
      s = q + alpha * ( q - q_prev )

      # Record current iteration for next momentum:
      p_prev = p
      q_prev = q

   x = L( p, q )
   x *= -l
   x += b
   return x
