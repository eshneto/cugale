#   Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
#
#   This file is part of cuGALE.
#
#   cuGALE is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   cuGALE is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with cuGALE.  If not, see <https://www.gnu.org/licenses/>.

import ctypes
import ctypes.util
import numpy
import math

libgale  = ctypes.CDLL( "./ccuGALE.so" )

import torch

_c_double_p  = ctypes.POINTER( ctypes.c_double )
_c_float_p  = ctypes.POINTER( ctypes.c_float )
_c_complex_p = ctypes.c_int64
_c_plan_p = ctypes.c_void_p

libgale.cuGALE_create.argtypes = [ ctypes.c_int,    # m
                                   ctypes.c_int,    # n
                                   ctypes.c_int,    # b
                                   ctypes.c_int,    # M
                                   ctypes.c_int,    # N
                                   _c_double_p,     # angles
                                   ctypes.c_int,    # S
                                   ctypes.c_int,    # N_L
                                   ctypes.c_double, # sigma_h
                                   ctypes.c_double, # sigma_v
                                   ctypes.c_double  # epsilon
                                 ]
libgale.cuGALE_create.restype = _c_plan_p

libgale.cuGALE_forward.argtypes = [ _c_plan_p,    # cuGALE object returned by cuGALE_create
                                    _c_complex_p, # in
                                    _c_complex_p, # out
                                    _c_complex_p  # buffer
                                  ]
libgale.cuGALE_forward.restype = None

libgale.cuGALE_adjoint.argtypes = [ _c_plan_p,    # cuGALE object returned by cuGALE_create
                                    _c_complex_p, # in
                                    _c_complex_p, # out
                                    _c_complex_p  # buffer
                                  ]
libgale.cuGALE_adjoint.restype = None

libgale.cuGALE_destroy.argtypes = [ _c_plan_p # cuGALE object returned by cuGALE_create
                                  ]
libgale.cuGALE_destroy.restype = None

def generate_golden_angles( N, first_angle = None ):

   if first_angle is None:
      first_angle = 0.0

   # Golden ratio:
   ratio = 2.0 * math.pi / ( 1.0 + ( 5.0 ** 0.5 ) )

   # Generate angles:
   angles = []
   for J in range( N ):
      angles.append( ratio * J + first_angle )

   # Constrain to ( pi/4, 5pi / 4]:
   for J in range( N ):
      curr_angle = angles[ J ] - ( math.pi / 4.0 )
      mul = int( curr_angle / math.pi )
      curr_angle -= mul * math.pi
      if curr_angle <= 0.0:
         curr_angle += math.pi
      angles[ J ] = curr_angle + ( math.pi / 4.0 )

   angles.sort()

   return angles

def generate_samples( M, angles, sigma = None, return_complex = True ):

   if isinstance( angles, ( int, long ) ):
      angles = generate_golden_angles( angles )

   if sigma is None:
      sigma = math.pi / M

   delta = 2.0 * math.pi / M

   xi = numpy.zeros( ( M, len( angles ) ) )
   upsilon = numpy.zeros( ( M, len( angles ) ) )

   for j in range( len( angles ) ):
      if ( abs( math.sin( angles[ j ] ) ) > abs( math.cos( angles[ j ] ) ) ) and \
         ( angles[ j ] <= 3.0 * math.pi / 4.0 ):
         # "Vertical" angles:
         ct = math.cos( angles[ j ] ) / math.sin( angles[ j ] )
         for i in range( M ):
            upsilon[ i, j ] = math.pi - delta * i - sigma;
            xi[ i, j ] = upsilon[ i, j ] * ct;

      else:
         # "Horizontal" angles:
         tg = math.tan( angles[ j ] )
         for i in range( M ):
            xi[ i, j ] = -math.pi + delta * i + sigma;
            upsilon[ i, j ] = xi[ i, j ] * tg

   if return_complex:
      return ( upsilon + 1j * xi ) / ( 2.0 * math.pi );
   else:
      return ( xi, upsilon )

class cuGALE( object ):

   def __init__( self, m, N_a = None, b = 1, n = None, M = None, N = None, S = 3, angles = None, P = None, sigma_h = None, sigma_v = None, epsilon = None ):

      if N_a is None:
         if angles is None:
            N_a = m
         else:
            if len( angles.shape ) != 1:
               raise ValueError( "angles must be an one-dimensional array." )
            N_a = len( angles )
      else:
         if not ( angles is None ):
            raise ValueError( "It is not allowed to provide both angles and N_a." )

      if n is None:
         n = m

      if M is None:
         M = m

      if ( not ( N is None ) ) and ( not ( P is None ) ):
         raise ValueError( "It is not allowed to provide both P and N." )

      if not ( P is None ):
         N = 2 * ( P - 2 * ( S + 1 ) )
      if N is None:
         N = int( round( 1.5 * 2 * max( [ m, n ] ) ) )

      if sigma_h is None:
         sigma_h = math.pi / M
      if sigma_v is None:
         sigma_v = sigma_h

      if epsilon is None:
         epsilon = 1.0 - 1.0e-4

      self.m = m
      self.n = n
      self.b = b
      self.M = M
      self.N_a = N_a
      self.N = N
      self.S = S
      self.sigma_h = sigma_h
      self.sigma_v = sigma_v
      self.epsilon = epsilon
      if angles is None:
         self.angles = numpy.array( generate_golden_angles( N_a ), dtype = numpy.float64 )
      else:
         self.angles = numpy.array( angles, dtype = numpy.float64 )

      self.plan = libgale.cuGALE_create( m, n, b, M, N_a, self.angles.ctypes.data_as( _c_double_p ), S, N, sigma_h, sigma_v, epsilon )
      self.intermediate = torch.empty( ( b, M, 2 * ( N + 4 * ( S + 1 ) ) ), device = 'cuda', dtype = torch.float32 )

   def forward( self, x, y = None ):

      if x.size() != ( self.b, self.m, 2 * self.n ):
         raise ValueError( "Wrong output tensor size! Should be ( %i, %i, 2 * %i )" % ( self.b, self.m, self.n ) );

      if y is None:
         y = torch.empty( ( self.b, self.M, 2 * self.N_a ), device = 'cuda', dtype = torch.float32 )

      if y.size() != ( self.b, self.M, 2 * self.N_a ):
         raise ValueError( "Wrong output tensor size! Should be ( %i, %i, 2 * %i )" % ( self.b, self.M, self.N_a ) );

      torch.cuda.synchronize()
      libgale.cuGALE_forward( self.plan, x.data_ptr(), y.data_ptr(), self.intermediate.data_ptr() )

      return y

   def adjoint( self, y, x = None ):

      if y.size() != ( self.b, self.M, 2 * self.N_a ):
         raise ValueError( "Wrong output tensor size! Should be ( %i, %i, 2 * %i )" % ( self.b, self.M, self.N_a ) );

      if x is None:
         x = torch.empty( ( self.b, self.m, 2 * self.n ), device = 'cuda', dtype = torch.float32 )

      if x.size() != ( self.b, self.m, 2 * self.n ):
         raise ValueError( "Wrong output tensor size! Should be ( %i, %i, 2 * %i )" % ( self.b, self.m, self.n ) );

      torch.cuda.synchronize()
      libgale.cuGALE_adjoint( self.plan, y.data_ptr(), x.data_ptr(), self.intermediate.data_ptr() )

      return x

   def __del__( self ):
      # If libgale has already gone out of scope we are about to be cleaned up anyway
      if not ( libgale is None ):
         libgale.cuGALE_destroy( self.plan )

def complex_abs( x ):
   return torch.sqrt( x[ :, :, ::2 ] ** 2 + x[ :, :, 1::2 ] ** 2 )

if __name__ == '__main__':

   import torch
   import matplotlib.pyplot as pp

   print 'Initializing GALE object on the GPU...'
   cugale_obj = cuGALE( 256, N_a = 128, b = 64, M = 512 )
   print 'Done!'

   print 'Initializing torch tensor on the GPU...'
   x = torch.zeros( ( cugale_obj.b, cugale_obj.m, 2 * cugale_obj.n ), device = 'cuda', dtype = torch.float32 )
   x[ :,
      cugale_obj.m / 4 : 3 * cugale_obj.m / 4,
      2 * cugale_obj.n / 4 : 2 * 3 * cugale_obj.n / 4 ] = 1.0
   print 'Done!'

   pp.figure()
   pp.imshow( complex_abs( x )[ 32, :, : ] )
   pp.colorbar()

   print 'Computing DFT on the GPU...'
   y = cugale_obj.forward( x )
   print 'Done!'

   pp.figure()
   pp.imshow( torch.log( complex_abs( y )[ 0, :, : ] ) )
   pp.colorbar()

   print 'Computing adjoint DFT on the GPU...'
   x = cugale_obj.adjoint( y )
   print 'Done!'

   pp.figure()
   pp.imshow( complex_abs( x )[ 32, :, : ] )
   pp.colorbar()

   pp.show()
