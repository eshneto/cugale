import binio
import matplotlib.pyplot as pp
import numpy

gpu_dtype = 'complex64'

# Load CPU computed DFT:
img_cpu = binio.load_bin( 'cpu_out.bin', dtype = 'complex128' )
pp.imsave( 'cpu.jpg', numpy.log10( numpy.abs( img_cpu ) ) )

# Load GPU computed DFT:
img_gpu = binio.load_bin( 'gpu_out.bin', dtype = gpu_dtype )
pp.imsave( 'gpu.jpg', numpy.log10( numpy.abs( img_gpu ) ) )

# Compute relative differences between CPU and GPU results:
# notice that we are using single precision on the GPU, but
# CPU computations use double precision.
img_diff = numpy.abs( img_cpu - img_gpu ) / numpy.abs( img_cpu )
pp.imsave( 'diff.jpg', numpy.log10( img_diff ) )

print 'Forward maximum relative difference = %f%%.' % ( img_diff.max() * 100, )
print 'Forward mean relative difference = %f%%.' % ( img_diff.mean() * 100, )

# Load CPU computed adjoint DFT:
adjimg_cpu = binio.load_bin( 'cpu_adjout.bin', dtype = 'complex128' )
pp.imsave( 'adjcpu.jpg', numpy.log10( numpy.abs( adjimg_cpu ) ) )

# Load GPU computed adjoint DFT:
adjimg_gpu = binio.load_bin( 'gpu_adjout.bin', dtype = gpu_dtype )
pp.imsave( 'adjgpu.jpg', numpy.log10( numpy.abs( adjimg_gpu ) ) )

# Compute relative differences between CPU and GPU results:
# notice that we are using single precision on the GPU, but
# CPU computations use double precision.
adjimg_diff = numpy.abs( adjimg_cpu - adjimg_gpu ) / numpy.abs( adjimg_cpu )
pp.imsave( 'adjdiff.jpg', numpy.log10( adjimg_diff ) )

print 'Adjoint maximum relative difference = %f%%.' % ( adjimg_diff.max() * 100, )
print 'Adjoint mean relative difference = %f%%.' % ( adjimg_diff.mean() * 100, )
