#ifndef CUDA_ARRAY_H
#define CUDA_ARRAY_H

#include "utils.h"

template < class data_t >
struct cuda_array{

   typedef data_t data_type;

   int n_;
   data_type * host_data_;
   data_type * device_data_;

   inline cuda_array( int n )
      : n_( n ), host_data_( 0 ), device_data_( 0 )
   {}

   inline void device_free()
   {
      if ( device_data_ )
      {
         cudaFree( device_data_ );
         utils::cuda_sync_check();
         device_data_ = 0;
      }
   }
   inline void host_free()
   {
      if ( host_data_ )
      {
         delete[] host_data_;
         host_data_ = 0;
      }
   }

   inline void device_alloc()
   {
      device_free();
      cudaMalloc( ( void * * ) & device_data_, sizeof( data_type ) * n_ );
      utils::cuda_sync_check();
   }
   inline void host_alloc()
   {
      host_free();
      host_data_ = new data_type[ n_ ];
   }

   inline void device_host()
   {
      cudaMemcpy( host_data_, device_data_, n_ * sizeof( data_type ), cudaMemcpyDeviceToHost );
      utils::cuda_sync_check();
   }

   inline void host_device()
   {
      cudaMemcpy( device_data_, host_data_, n_ * sizeof( data_type ), cudaMemcpyHostToDevice );
      utils::cuda_sync_check();
   }

   inline ~cuda_array()
   {
      device_free();
      host_free();
   }

};

#endif // #ifndef CUDA_ARRAY_H
