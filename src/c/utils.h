#ifndef UTILS_H
#define UTILS_H

#include <exception>
#include <cufftXt.h>
#include <cublas_v2.h>
#include <complex>
#include <chrono>
#include <iostream>
#include <fstream>

namespace utils {

   /**
    * Class template for uniformization of precision-dependent cufft types and constants:
    */
   template < class data_t >
   struct cufft_type {
      // Error, data_t should be one of "float" or "double".
   };

   template <>
   struct cufft_type< float > {
      typedef float data_type;
      typedef cufftComplex complex_type; ///< cuFFT's single-precision complex type.
      static cufftType_t const transform_type = CUFFT_C2C;  ///< The complex to complex single-precision cuFFT transform.
      /**
       * Call single-precision execute function with given parameters:
       */
      inline static cufftResult_t execute( cufftHandle plan,
                                           std::complex< data_type > * idata,
                                           std::complex< data_type > * odata,
                                           int direction
      )
      {
         return cufftExecC2C( plan,
                              reinterpret_cast< complex_type * >( idata ),
                              reinterpret_cast< complex_type * >( odata ),
                              direction
                            );
      }
      typedef cufftComplex (*callback_type)(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer);
   };

   template <>
   struct cufft_type< double > {
      typedef double data_type;
      typedef cufftDoubleComplex complex_type;
      static cufftType_t const transform_type = CUFFT_Z2Z;
      inline static cufftResult_t execute( cufftHandle plan,
                                           std::complex< data_type > * idata,
                                           std::complex< data_type > * odata,
                                           int direction
      )
      {
         return cufftExecZ2Z( plan,
                              reinterpret_cast< complex_type * >( idata ),
                              reinterpret_cast< complex_type * >( odata ),
                              direction
                            );
      }
      typedef cufftDoubleComplex (*callback_type)(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer);
   };

   struct time {
      static std::chrono::steady_clock::time_point begin;
      static std::chrono::steady_clock::time_point end;
      static void tic() {
         begin = std::chrono::steady_clock::now();
      }
      static double toc() {
         end = std::chrono::steady_clock::now();
         return std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1.0e6;
      }
      static void report()
      {
         std::cout << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
      }
      static double tocr() {
         double retval = toc();
         report();
         return retval;
      }
   };

   std::chrono::steady_clock::time_point time::begin;
   std::chrono::steady_clock::time_point time::end;

   template < class data_t >
   struct cuda_math {
      // Error, some specialization should be loaded.
   };

   template <>
   struct cuda_math< float > {
      typedef float data_type;
      __device__ inline static void sin_cos( data_type x, data_type * s, data_type * c )
      {
         sincosf( x, s, c );
      }
   };

   template <>
   struct cuda_math< double > {
      typedef double data_type;
      __device__ inline static void sin_cos( data_type x, data_type * s, data_type * c )
      {
         sincos( x, s, c );
      }
   };

   inline char const * cufft_error_string( cufftResult_t result )
   {
      switch ( result )
      {
         case CUFFT_INVALID_PLAN:
            return "CUFFT_INVALID_PLAN";
         case CUFFT_ALLOC_FAILED:
            return "CUFFT_ALLOC_FAILED";
         case CUFFT_INVALID_VALUE:
            return "CUFFT_INVALID_VALUE";
         case CUFFT_INTERNAL_ERROR:
            return "CUFFT_INTERNAL_ERROR";
         case CUFFT_SETUP_FAILED:
            return "CUFFT_SETUP_FAILED";
         case CUFFT_INVALID_SIZE:
            return "CUFFT_INVALID_SIZE";
         default:
            return "UNKNOWN ERROR";
      }
   }

   inline void cufft_check( cufftResult_t result )
   {
      if ( result != CUFFT_SUCCESS )
      {
         throw std::runtime_error( cufft_error_string( result ) );
      }
   }

   inline char const * cuda_error_string( cudaError_t result )
   {
      return cudaGetErrorString( result );
   }

   inline void cuda_sync_check()
   {
      cudaDeviceSynchronize();

      cudaError_t result = cudaGetLastError();
      if ( result != cudaSuccess )
      {
         throw std::runtime_error( cuda_error_string( result ) );
      }
   }

   inline void cuda_sync_check( cudaError_t result )
   {
      cudaDeviceSynchronize();
      if ( result != cudaSuccess )
      {
         throw std::runtime_error( cuda_error_string( result ) );
      }
   }

   inline char const * cublas_error_string( cublasStatus_t result )
   {
      switch ( result )
      {
         case CUBLAS_STATUS_NOT_INITIALIZED:
            return "CUBLAS_STATUS_NOT_INITIALIZED";
         case CUBLAS_STATUS_ALLOC_FAILED:
            return "CUBLAS_STATUS_ALLOC_FAILED";
         case CUBLAS_STATUS_INVALID_VALUE:
            return "CUBLAS_STATUS_INVALID_VALUE";
         case CUBLAS_STATUS_ARCH_MISMATCH:
            return "CUBLAS_STATUS_ARCH_MISMATCH";
         case CUBLAS_STATUS_MAPPING_ERROR:
            return "CUBLAS_STATUS_MAPPING_ERROR";
         case CUBLAS_STATUS_EXECUTION_FAILED:
            return "CUBLAS_STATUS_EXECUTION_FAILED";
         case CUBLAS_STATUS_INTERNAL_ERROR:
            return "CUBLAS_STATUS_INTERNAL_ERROR";
         default:
            return "UNKNOWN ERROR";
      }
   }

   inline void cublas_check( cublasStatus_t result )
   {
      if ( result != CUBLAS_STATUS_SUCCESS )
      {
         throw std::runtime_error( cublas_error_string( result ) );
      }
   }

   size_t device_mem_check()
   {
      size_t free_memory, total_memory;

      cudaMemGetInfo( & free_memory, & total_memory );
      cuda_sync_check();

      static double unit = 1024.0 * 1024.0 * 1024.0;

      std::cout << "GPU Memory: " << free_memory / unit << "GB available out of " << total_memory / unit << "GB total\n";

      return free_memory;
   }

   template < class data_t >
   struct pi {
      static constexpr data_t value = static_cast< data_t >( 3.141592653589793238462643383279502884L );
      static constexpr data_t twice = static_cast< data_t >( 6.283185307179586476925286766559005768L );
   };

   template < class data_t >
   void save_array( char const * filename, int m, int n, data_t * img )
   {
      std::ofstream file( filename, std::ios::binary );
      file.write( reinterpret_cast< char* >( &m ), sizeof( int ) );
      file.write( reinterpret_cast< char* >( &n ), sizeof( int ) );

      file.write( reinterpret_cast< char* >( img ), m * n * sizeof( data_t ) );
   }
} // namespace cuda
# endif // #ifndef CUDAUTILS_H
