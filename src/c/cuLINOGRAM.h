#ifndef CULINOGRAM_H
#define CULINOGRAM_H

#include "cuLINOGRAM_kernels.h"
#include "cuCZT.h"
#include "cuda_array.h"
#include <iostream>
#include <iomanip>

template < class data_t >
struct cuLINOGRAM {

   typedef data_t data_type;

   cuCZT< data_t > czt_obj_vertical_;
   cuCZT< data_t > czt_obj_horizontal_;

   int m_;
   int n_;
   int M_;
   int P_;
   int b_;
   int row_length_;

   data_t sigma_h_;
   data_t sigma_v_;

   cufftHandle vertical_plan_;
   cufftHandle horizontal_plan_;
   cufftHandle adjoint_vertical_plan_;
   cufftHandle adjoint_horizontal_plan_;

   std::complex< data_t > * cufft_workspace_;
   std::complex< data_t > * cuczt_workspace_;

   cuLINOGRAM( int m, int n, int M, int N, int S, int b );
   cuLINOGRAM( int m, int n, int M, int N, int S, int b, data_t sigma_h, data_t sigma_v );

   void forward_vertical( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer );
   void forward_horizontal( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer );
   void forward( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer );

   void adjoint_vertical( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer );
   void adjoint_horizontal( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer );
   void adjoint( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer );

   ~cuLINOGRAM();
};

/**
 * \brief Constructor
 *
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray. Must be an even number and must be larger than or equal the maximum between \p m and \p n.
 * \param N Number of pseudo-polar rays. Must be an even number and must be larger than or equal 2 times the maximum between \p m and \p n.
 * \param S 1/4 minus one of the number of extra rays.
 * \param b Number of images in batch.
 **/
template < class data_t >
cuLINOGRAM< data_t >::cuLINOGRAM( int m, int n, int M, int N, int S, int b, data_t sigma_h, data_t sigma_v )
   : czt_obj_horizontal_( m, M, N / 2 + 2 * ( S + 1 ), 2 * ( S + 1 ), b, M / 2, N / 4 + S, -sigma_h ),
     // Notice that shifts for the vertical chirp-z are inverted so we can match the
     // order convention of the CPU implementation (which in there requires weird memory copies).
     czt_obj_vertical_( n, M, N / 2 + 2 * ( S + 1 ), 2 * ( S + 1 ), b, M / 2, N / 4 + S, -sigma_v ),
     m_( m ), n_( n ), M_( M ), P_( N / 2 + 2 * ( S + 1 ) ), b_( b ), row_length_( 2 * P_ ),
     sigma_h_( sigma_h ), sigma_v_( sigma_v )
{
   static cufftType_t const transform_type = utils::cufft_type< data_type >::transform_type; ///< Type of cuFFT transform.

   // Create plans:
   utils::cufft_check(
      cufftCreate( &vertical_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( vertical_plan_, 0 )
   );
   size_t vertical_plan_size;
   // Vertical plan:
   // Input:  zero-padded M_ columns and M_ rows array
   // Output: zero-padded 2 * P_ columns and M_ rows array
   int embed[] = { n_ * b_ }; //< Logical input embedding.
   utils::cufft_check(
      // Creates a plan for n*b 1D FFTs of length M:
      cufftMakePlanMany( vertical_plan_,  ///< Plan.
                         1,               ///< Transforms rank.
                         &M_,             ///< Individual transforms dimensions.
                         embed,           ///< Input embedding.
                         embed[ 0 ],      ///< Input stride.
                         1,               ///< Input distance.
                         embed,           ///< Output embedding.
                         embed[ 0 ],      ///< Output stride.
                         1,               ///< Output distance.
                         transform_type,  ///< Transform type.
                         n_ * b_,         ///< Batch size.
                         &vertical_plan_size
                       )
   );

   // Create plans:
   utils::cufft_check(
      cufftCreate( &horizontal_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( horizontal_plan_, 0 )
   );
   size_t horizontal_plan_size;
   // Vertical plan:
   // Input:  zero-padded M_ columns and M_ rows array
   // Output: zero-padded 2 * P_ columns and M_ rows array
   embed[ 0 ] = m_ * b_;
   utils::cufft_check(
      // Creates a plan for n*b 1D FFTs of length M:
      cufftMakePlanMany( horizontal_plan_,///< Plan.
                         1,               ///< Transforms rank.
                         &M_,             ///< Individual transforms dimensions.
                         embed,           ///< Input embedding.
                         1,               ///< Input stride.
                         M_,              ///< Input distance.
                         embed,           ///< Output embedding.
                         embed[ 0 ],      ///< Output stride.
                         1,               ///< Output distance.
                         transform_type,  ///< Transform type.
                         m_ * b_,         ///< Batch size.
                         &horizontal_plan_size
                       )
   );

   utils::cufft_check(
      cufftCreate( &adjoint_vertical_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( adjoint_vertical_plan_, 0 )
   );
   size_t adjoint_vertical_plan_size;
   embed[ 0 ] = n_ * b_; //< Logical input embedding.
   utils::cufft_check(
      // Creates a plan for n*b 1D FFTs of length M:
      cufftMakePlanMany( adjoint_vertical_plan_,  ///< Plan.
                         1,               ///< Transforms rank.
                         &M_,             ///< Individual transforms dimensions.
                         embed,           ///< Input embedding.
                         embed[ 0 ],      ///< Input stride.
                         1,               ///< Input distance.
                         embed,           ///< Output embedding.
                         embed[ 0 ],      ///< Output stride.
                         1,               ///< Output distance.
                         transform_type,  ///< Transform type.
                         n_ * b_,         ///< Batch size.
                         &adjoint_vertical_plan_size
                       )
   );

   utils::cufft_check(
      cufftCreate( &adjoint_horizontal_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( adjoint_horizontal_plan_, 0 )
   );
   size_t adjoint_horizontal_plan_size;
   // Vertical plan:
   // Input:  zero-padded M_ columns and M_ rows array
   // Output: zero-padded 2 * P_ columns and M_ rows array
   embed[ 0 ] = m_ * b_;
   utils::cufft_check(
      // Creates a plan for n*b 1D FFTs of length M:
      cufftMakePlanMany( adjoint_horizontal_plan_,///< Plan.
                         1,               ///< Transforms rank.
                         &M_,             ///< Individual transforms dimensions.
                         embed,           ///< Input embedding.
                         embed[ 0 ],      ///< Input stride.
                         1,               ///< Input distance.
                         embed,           ///< Output embedding.
                         1,               ///< Output stride.
                         M_,              ///< Output distance.
                         transform_type,  ///< Transform type.
                         m_ * b_,         ///< Batch size.
                         &adjoint_horizontal_plan_size
                       )
   );

   // Allocate workspace for FFTs:
   size_t plan_size = std::max( std::max( vertical_plan_size, horizontal_plan_size ),
                                std::max( adjoint_vertical_plan_size, adjoint_horizontal_plan_size )
                              );
   cudaMalloc( reinterpret_cast< void ** >( &cufft_workspace_ ), plan_size );
   utils::cuda_sync_check();

   // Set workspace:
   utils::cufft_check(
      cufftSetWorkArea( vertical_plan_, cufft_workspace_ )
   );
   utils::cufft_check(
      cufftSetWorkArea( horizontal_plan_, cufft_workspace_ )
   );
   utils::cufft_check(
      cufftSetWorkArea( adjoint_vertical_plan_, cufft_workspace_ )
   );
   utils::cufft_check(
      cufftSetWorkArea( adjoint_horizontal_plan_, cufft_workspace_ )
   );

   // Allocate workspace for CZTs:
   cudaMalloc( reinterpret_cast< void ** >( &cuczt_workspace_ ), b * row_length_ * M_ * sizeof( std::complex< data_type > ) );
   utils::cuda_sync_check();

   utils::li_callback_setter< data_type >::set_vertical_shift_factors_multiply( vertical_plan_ );
   utils::li_callback_setter< data_type >::set_vertical_store( vertical_plan_ );
   utils::li_callback_setter< data_type >::set_horizontal_shift_factors_multiply( horizontal_plan_ );
   utils::li_callback_setter< data_type >::set_horizontal_store( horizontal_plan_ );
   utils::li_callback_setter< data_type >::set_vertical_load( adjoint_vertical_plan_ );
   utils::li_callback_setter< data_type >::set_vertical_shift_factors_conjugate_multiply( adjoint_vertical_plan_ );
   utils::li_callback_setter< data_type >::set_horizontal_load( adjoint_horizontal_plan_ );
   utils::li_callback_setter< data_type >::set_horizontal_shift_factors_conjugate_multiply_sum( adjoint_horizontal_plan_ );
}

template< class data_t >
void cuLINOGRAM< data_t >::forward_vertical( std::complex< data_t > * in,
                                             std::complex< data_t > * out,
                                             std::complex< data_t > * buffer
                                           )
{
   utils::linogram_to_constant_memory( row_length_, m_, n_, M_, b_, data_type( M_ / 2 ), sigma_v_, out );

   // Run DFT of the data. Once more, the direction is reversed so we can invert
   // the order of the result so to follow previous conventions.
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( vertical_plan_, in, buffer, CUFFT_INVERSE )
   );
   utils::cuda_sync_check();

   // Run chirp-z of the DFTed data:
   czt_obj_vertical_.forward( buffer, out, cuczt_workspace_ );
}

template< class data_t >
void cuLINOGRAM< data_t >::forward_horizontal( std::complex< data_t > * in,
                                               std::complex< data_t > * out,
                                               std::complex< data_t > * buffer
                                             )
{
   utils::linogram_to_constant_memory( row_length_, m_, n_, M_, b_, data_type( M_ / 2 ), sigma_h_, out );

   utils::cufft_check(
      utils::cufft_type< data_type >::execute( horizontal_plan_, in, buffer, CUFFT_FORWARD )
   );
   utils::cuda_sync_check();

   // Run chirp-z of the DFTed data:
   czt_obj_horizontal_.forward( buffer, out, cuczt_workspace_ );
}

template< class data_t >
void cuLINOGRAM< data_t >::forward( std::complex< data_t > * in,
                                    std::complex< data_t > * out,
                                    std::complex< data_t > * buffer
                                  )
{
   forward_vertical( in, out, buffer );
   forward_horizontal( in, out + P_, buffer );
}

template< class data_t >
void cuLINOGRAM< data_t >::adjoint_vertical( std::complex< data_t > * in,
                                             std::complex< data_t > * out,
                                             std::complex< data_t > * buffer
                                           )
{
   utils::linogram_to_constant_memory( row_length_, m_, n_, M_, b_, data_type( M_ / 2 ), sigma_v_, out );

   // Run adjoint chirp-z of data:
   czt_obj_vertical_.adjoint( in, buffer, cuczt_workspace_ );

   // Run DFT of the data. Once more, the direction is reversed so we can invert
   // the order of the result so to follow previous conventions.
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( adjoint_vertical_plan_, buffer, cuczt_workspace_, CUFFT_FORWARD )
   );
   utils::cuda_sync_check();
}

template< class data_t >
void cuLINOGRAM< data_t >::adjoint_horizontal( std::complex< data_t > * in,
                                               std::complex< data_t > * out,
                                               std::complex< data_t > * buffer
                                             )
{
   utils::linogram_to_constant_memory( row_length_, m_, n_, M_, b_, data_type( M_ / 2 ), sigma_h_, out );

   // Run adjoint chirp-z of data:
   czt_obj_horizontal_.adjoint( in, buffer, cuczt_workspace_ );

   utils::cufft_check(
      utils::cufft_type< data_type >::execute( adjoint_horizontal_plan_, buffer, cuczt_workspace_, CUFFT_INVERSE )
   );
   utils::cuda_sync_check();
}

template< class data_t >
void cuLINOGRAM< data_t >::adjoint( std::complex< data_t > * in,
                                    std::complex< data_t > * out,
                                    std::complex< data_t > * buffer
                                  )
{
   adjoint_vertical( in, out, buffer );
   adjoint_horizontal( in + P_, out, buffer );
}

template< class data_t >
cuLINOGRAM< data_t >::~cuLINOGRAM()
{
   cudaFree( cufft_workspace_ );
   utils::cuda_sync_check();
   cudaFree( cuczt_workspace_ );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( vertical_plan_ )
   );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( adjoint_vertical_plan_ )
   );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( horizontal_plan_ )
   );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( adjoint_horizontal_plan_ )
   );
   utils::cuda_sync_check();
}

#endif // #ifndef CULINOGRAM_H
