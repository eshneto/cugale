#ifndef CU_KB_H
#define CU_KB_H

#include <stdio.h>

namespace cukb {

   // Template for simple constants:
   template < class d_type >
   struct const_math {
   };
   template <>
   struct const_math< double > {
      typedef double data_type;
      static constexpr data_type zero = 0;
      static constexpr data_type one = 1;
      static constexpr data_type two = 2;
   };
   template <>
   struct const_math< float > {
      typedef float data_type;
      static constexpr data_type zero = 0;
      static constexpr data_type one = 1;
      static constexpr data_type two = 2;
   };

   // Template for mathematical functions:
   template < class d_type >
   struct cuda_math {
   };
   template <>
   struct cuda_math< double > {
      typedef double data_type;
      inline static __device__ data_type abs( data_type x )
      {
         return fabs( x );
      }
      inline static __device__ data_type sqrt( data_type x )
      {
         return ::sqrt( x );
      }
      inline static __device__ data_type sin( data_type x )
      {
         return sin( x );
      }
      inline static __device__ data_type sinh( data_type x )
      {
         return sinh( x );
      }
      inline static __device__ data_type bessel( data_type x )
      {
         return cyl_bessel_i0( x );
      }
   };
   template <>
   struct cuda_math< float > {
      typedef float data_type;
      inline static __device__ data_type abs( data_type x )
      {
         return fabsf( x );
      }
      inline static __device__ data_type sqrt( data_type x )
      {
         return sqrtf( x );
      }
      inline static __device__ data_type sin( data_type x )
      {
         return sinf( x );
      }
      inline static __device__ data_type sinh( data_type x )
      {
         return sinhf( x );
      }
      inline static __device__ data_type bessel( data_type x )
      {
         return cyl_bessel_i0f( x );
      }
   };

   /**
    * \brief Kaiser-Bessel window function.
    *
    * \tparam d_type Floating point type.
    * \param alpha Main lobe halfwidth parameter. In the frequency domain, it determines the trade-off between main-lobe width and side lobe level.
    * \param tau Support parameter.
    * \param t Argument.
    *
    * The Kaiser-Bessel window function \f$K_{\alpha, \tau}\f$ has a good compromise between small support and fast Fourier decay, and is given by
    * \f[
    *    K_{\alpha, \tau}( t ) := \begin{cases}
    *                                \displaystyle\frac{I_0( \alpha\sqrt{1 - (t / \tau)^2} )}{I_0( \alpha )} & | t | \leq | \tau |\\
    *                                0                                                          & | t | > | \tau |,
    *                             \end{cases}
    * \f]
    * where \f$I_0\f$ is the zero-order modified cylindrical Bessel function, \f$\alpha = {}\f$ \p alpha, \f$\tau = {}\f$ \p tau, and \f$t = {}\f$ \p t.
    */
   template < class d_type = double >
   __device__ d_type kaiser_bessel( d_type alpha, d_type tau, d_type t )
   {
      if ( cuda_math< d_type >::abs( t ) >
           cuda_math< d_type >::abs( tau )
         )
      {
         return  const_math< d_type >::zero;
      }
      else
      {
         d_type tmp = t / tau; tmp *= tmp;
         tmp = alpha * cuda_math< d_type >::sqrt( const_math< d_type >::one - tmp );
         return cuda_math< d_type >::bessel( tmp ) / cuda_math< d_type >::bessel( alpha );
      }
   }

   /**
    * \brief Fourier Transform of Kaiser-Bessel window function.
    *
    * \tparam d_type Floating point type.
    * \param alpha Main lobe halfwidth parameter. In the frequency domain, it determines the trade-off between main-lobe width and side lobe level.
    * \param tau Support parameter.
    * \param omega Argument.
    *
    * Fourier Transform \f$\hat K_{\alpha, \tau}\f$ of the Kaiser-Bessel window function \f$K_{\alpha, \tau}\f$ (see kb::kaiser_bessel()), given by:
    * \f[
    *    \hat K_{\alpha, \tau}( t ) = \frac{2\tau}{I_0( \alpha )}\frac{\sinh(\alpha\sqrt{ 1 - (\omega\tau / \alpha )^2 })}{\alpha\sqrt{ 1 - (\omega\tau / \alpha )^2 }}
    * \f]
    * where \f$I_0\f$ is the zero-order modified cylindrical Bessel function, \f$\alpha = {}\f$ \p alpha, \f$\tau = {}\f$ \p tau, and \f$\omega = {}\f$ \p omega.
    */
   template < class d_type = double >
   __device__ d_type fourier_kaiser_bessel( d_type alpha, d_type tau, d_type omega )
   {
      d_type const factor = ( const_math< d_type >::two * tau ) / cuda_math< d_type >::bessel( alpha );

      d_type tmp = ( omega * tau ) / alpha; tmp *= tmp;
      tmp = const_math< d_type >::one - tmp;

      if ( tmp < 0 )
      {
         tmp = alpha * cuda_math< d_type >::sqrt( -tmp );
         return factor * ( cuda_math< d_type >::sin( tmp ) / tmp );
      }
      else if ( tmp > 0 )
      {
         tmp = alpha * cuda_math< d_type >::sqrt( tmp );
         return factor * ( cuda_math< d_type >::sinh( tmp ) / tmp );
      }
      else
      {
         return factor;
      }
   }
} // namespace kb

#endif // #ifndef CU_KB_H
