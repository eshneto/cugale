#ifndef CUCZT_KERNELS_H
#define CUCZT_KERNELS_H

#include <complex>
#include "utils.h"
#include "my_complex.h"

namespace utils {

   template < class data_t >
   __device__ inline data_t alpha_I( int I, int M, data_t R, data_t sigma )
   {
      return static_cast< data_t >( 4.0 ) * ( I - R ) / M - static_cast< data_t >( 2.0 ) * sigma / pi< data_t >::value;
   }

   // A call to this kernel should use a grid of M x b blocks. Each block will process
   // a line of at most 1024 elements. The blocks should be linear and code must be called
   // repeated times if m > 1024. Repeated call is responsibility of host for performance
   // reasons because it is best to branch in host code than branching later in device code.
   template < class data_t >
   __global__ void shift_factors_compute( std::complex< data_t > * shift_factors,
                                          int M, int m,
                                          data_t R_1,
                                          data_t R_2,
                                          data_t sigma,
                                          data_t den,
                                          int i_0
                                        )
   {
      // Compute constants:
      data_t aux = pi< data_t >::value * alpha_I( blockIdx.x, M, R_1, sigma ) / den;
      data_t R2 = static_cast< data_t >( 2.0 ) * R_2;

      // Compute shift factor:
      int i = threadIdx.x + i_0;
      my_complex< data_t > & sf = ref_complex( shift_factors[ blockIdx.x * m + i ] );
      cuda_math< data_t >::sin_cos( i * aux * ( R2 - i ),
                                    &( sf.y ),
                                    &( sf.x )
                                  );
   }

   // A call to this kernel should use a grid of M blocks. Each block will process
   // a line of at most 1024 elements. The blocks should be linear and code must be called
   // repeated times if P > 1024. Repeated call is responsibility of host for performance
   // reasons since it is best to branch in host code than branching later in device code.
   template < class data_t >
   __global__ void chirp_factors_compute_first_half( std::complex< data_t > * data,
                                                     int M,
                                                     int row_length,
                                                     data_t R_1,
                                                     data_t sigma,
                                                     data_t den,
                                                     int i_0
                                                   )
   {
      // Compute constant coefficient:
      data_t aux = pi< data_t >::value * alpha_I( blockIdx.x, M, R_1, sigma ) / den;

      // Process element:
      int i = threadIdx.x + i_0;
      // Start of line to be processed assuming 1D grid of size M and 1D blocks.
      my_complex< data_t > & datum_ref = ref_complex( data[ blockIdx.x * row_length + i ] );
      my_complex< data_t > datum;
      cuda_math< data_t >::sin_cos( ( i * i ) * aux,
                                    &( datum.y ),
                                    &( datum.x )
                                  );
      datum.x /= row_length;
      datum.y /= row_length;
      datum_ref = datum;
   }
   // A call to this kernel should use a grid of M blocks. Each block will process
   // a line of at most 1024 elements. The blocks should be linear and code must be called
   // repeated times if P > 1024. Repeated call is responsibility of host for performance
   // reasons since it is best to branch in host code than branching later in device code.
   template < class data_t >
   __global__ void chirp_factors_compute_second_half( std::complex< data_t > * data,
                                                      int M,
                                                      int P,
                                                      int row_length,
                                                      data_t R_1,
                                                      data_t sigma,
                                                      data_t den,
                                                      int i_0
                                                    )
   {
      // Compute constant coefficient:
      data_t aux = pi< data_t >::value * alpha_I( blockIdx.x, M, R_1, sigma ) / den;

      // Process element:
      int i = threadIdx.x + i_0 + P;
      // Start of line to be processed assuming 1D grid of size M and 1D blocks.
      my_complex< data_t > & datum_ref = ref_complex( data[ blockIdx.x * row_length + i ] );
      my_complex< data_t > datum;
      int i_val = row_length - i;
      cuda_math< data_t >::sin_cos( ( i_val * i_val ) * aux,
                                    &( datum.y ),
                                    &( datum.x )
                                  );
      datum.x /= row_length;
      datum.y /= row_length;
      datum_ref = datum;
   }

   template < class data_t >
   struct cuCZT_data {
      typedef data_t data_type; ///< Floating point type.

      int m_; ///< Input length.
      int M_; ///< Number of CZTs.
      int P_; ///< CZTs lengths.
      int E_; ///< "Extra" terms.
      int b_; ///< Batch size.
      int row_length_;
      int plane_area_;
      data_type R_1_; ///< Shift in row indexing.
      data_type R_2_; ///< Shift in column indexing.
      data_type sigma_; ///< In-ray shift of samples.
      data_type den_;
      std::complex< data_type > * chirp_factors_; ///< Chirp factors.
      std::complex< data_type > * shift_factors_; ///< Shift factors.

      cuCZT_data() {}

      cuCZT_data( int m,
                  int M,
                  int P,
                  int E,
                  int b,
                  data_type R_1,
                  data_type R_2,
                  data_type sigma
                )
      : m_( m ), M_( M ), P_( P ), E_( E ), b_( b ), row_length_( 2 * P_ ), plane_area_( row_length_ * M_ ),
      R_1_( R_1 ), R_2_( R_2 ), sigma_( sigma ), den_( 2.0 * ( P_ - E_ ) )
      {}
   };

   typedef union {
      float f;
      double d;
   } float_double;
   template < class data_t  >
   struct fd_extract {
   };
   template <>
   struct fd_extract< float > {
      __device__ static float & ref( float_double & x ) {
         return x.f;
      }
   };
   template <>
   struct fd_extract< double > {
      __device__ static double & ref( float_double & x ) {
         return x.d;
      }
   };

   // These variables will store values required for the
   // callback routines
   __constant__ void * cm_chirp_factors_;
   __constant__ void * cm_shift_factors_;
   __constant__ int cm_plane_area_;
   __constant__ int cm_row_length_;
   __constant__ int cm_m_;
   __constant__ int cm_M_;
   __constant__ int cm_P_;
   __constant__ float_double cm_R_1_;
   __constant__ float_double cm_R_2_;
   __constant__ float_double cm_den_;
   __constant__ float_double cm_sigma_;
   __constant__ void * cm_ptr_;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type CZCB_chirp_factors_multiply(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // In-plane data position:
      int cf_idx = idx % cm_plane_area_;

      typename cufft_type< data_t >::complex_type chirp_factor = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_chirp_factors_ )[ cf_idx ];
      typename cufft_type< data_t >::complex_type datum = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
      typename cufft_type< data_t >::complex_type retval = {
         datum.x * chirp_factor.x - datum.y * chirp_factor.y,
         datum.y * chirp_factor.x + datum.x * chirp_factor.y
      };

      return retval;
   }

   __device__ cufftCallbackLoadC d_CZCB_chirp_factors_multiply_float_ptr  = CZCB_chirp_factors_multiply< float >;
   __device__ cufftCallbackLoadZ d_CZCB_chirp_factors_multiply_double_ptr = CZCB_chirp_factors_multiply< double >;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type CZCB_chirp_factors_conjugate_multiply(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // In-plane data position:
      int cf_idx = idx % cm_plane_area_;

      typename cufft_type< data_t >::complex_type chirp_factor = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_chirp_factors_ )[ cf_idx ];
      typename cufft_type< data_t >::complex_type datum = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
      typename cufft_type< data_t >::complex_type retval = {
         datum.x * chirp_factor.x + datum.y * chirp_factor.y,
         datum.y * chirp_factor.x - datum.x * chirp_factor.y
      };

      return retval;
   }

   __device__ cufftCallbackLoadC d_CZCB_chirp_factors_conjugate_multiply_float_ptr  = CZCB_chirp_factors_conjugate_multiply< float >;
   __device__ cufftCallbackLoadZ d_CZCB_chirp_factors_conjugate_multiply_double_ptr = CZCB_chirp_factors_conjugate_multiply< double >;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type CZCB_shift_factors_multiply(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // In-plane data position:
      int column = idx % cm_row_length_;
      if ( column >= cm_m_ )
         return typename cufft_type< data_t >::complex_type{
            static_cast< data_t >( 0.0 ),
            static_cast< data_t >( 0.0 )
         };
      int row = ( idx / cm_row_length_ ) % cm_M_;

      // Image index in batch:
      int i_b = idx / cm_plane_area_;

      // Element in input data plane:
      idx = row * cm_m_ + column;
      // Shift factor in precomputed array:
      typename cufft_type< data_t >::complex_type shift_factor = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_shift_factors_ )[ idx ];
      typename cufft_type< data_t >::complex_type datum = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ i_b * cm_m_ * cm_M_ + idx ];

      typename cufft_type< data_t >::complex_type retval = {
         datum.x * shift_factor.x - datum.y * shift_factor.y,
         datum.y * shift_factor.x + datum.x * shift_factor.y
      };
      return retval;
   }

   __device__ cufftCallbackLoadC d_CZCB_shift_factors_multiply_float_ptr  = CZCB_shift_factors_multiply< float >;
   __device__ cufftCallbackLoadZ d_CZCB_shift_factors_multiply_double_ptr = CZCB_shift_factors_multiply< double >;

   // Store callback for adjoint chirp-z. There seems to be something wrong here as the whole output buffer
   // can be overwritten.
   template < class data_t >
   __device__ void CZCB_shift_factors_conjugate_multiply( void * data,
                                                          size_t idx,
                                                          typename cufft_type< data_t >::complex_type el,
                                                          void * callerInfo,
                                                          void * sharedPtr
                                                        )
   {
      // In-plane data position:
      int column = idx % cm_row_length_;
      if ( column >= cm_m_ )
         return;
      int row = ( idx / cm_row_length_ ) % cm_M_;

      // Image index in batch:
      int i_b = idx / cm_plane_area_;

      // Element in output data plane:
      idx = row * cm_m_ + column;
      // Shift factor in precomputed array:
      typename cufft_type< data_t >::complex_type shift_factor = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_shift_factors_ )[ idx ];
      reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_ptr_ )[ i_b * cm_m_ * cm_M_ + idx ] = {
         el.x * shift_factor.x + el.y * shift_factor.y,
         el.y * shift_factor.x - el.x * shift_factor.y
      };
   }

   __device__ cufftCallbackStoreC d_CZCB_shift_factors_conjugate_multiply_float_ptr  = CZCB_shift_factors_conjugate_multiply< float >;
   __device__ cufftCallbackStoreZ d_CZCB_shift_factors_conjugate_multiply_double_ptr = CZCB_shift_factors_conjugate_multiply< double >;

   template < class data_t >
   __device__ void CZCB_phase_factors_multiply( void * data,
                                                size_t idx,
                                                typename cufft_type< data_t >::complex_type el,
                                                void * callerInfo,
                                                void * sharedPtr
                                              )
   {
      // In-plane data position:
      int column = idx % cm_row_length_;
      if ( column >= cm_P_ )
         return;
      int line = ( idx / cm_row_length_ ) % cm_M_;

      // Compute constants:
      data_t aux = pi< data_t >::value * alpha_I( line,
                                                  cm_M_,
                                                  fd_extract< data_t >::ref( cm_R_1_ ),
                                                  fd_extract< data_t >::ref( cm_sigma_ )
                                                ) / fd_extract< data_t >::ref( cm_den_ );
      typename cufft_type< data_t >::complex_type phase_factor;
      cuda_math< data_t >::sin_cos( column * column * aux,
                                    &( phase_factor.y ),
                                    &( phase_factor.x )
                                  );

      reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_ptr_ )[ idx ] = {
         el.x * phase_factor.x + el.y * phase_factor.y,
         el.y * phase_factor.x - el.x * phase_factor.y
      };
   }

   __device__ cufftCallbackStoreC d_CZCB_phase_factors_multiply_float_ptr  = CZCB_phase_factors_multiply< float >;
   __device__ cufftCallbackStoreZ d_CZCB_phase_factors_multiply_double_ptr = CZCB_phase_factors_multiply< double >;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type CZCB_phase_factors_conjugate_multiply(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // In-plane data position:
      int column = idx % cm_row_length_;
      if ( column >= cm_P_ )
         return typename cufft_type< data_t >::complex_type{
            static_cast< data_t >( 0.0 ),
            static_cast< data_t >( 0.0 )
         };
      int line = ( idx / cm_row_length_ ) % cm_M_;

      // Compute constants:
      data_t aux = pi< data_t >::value * alpha_I( line,
                                                  cm_M_,
                                                  fd_extract< data_t >::ref( cm_R_1_ ),
                                                  fd_extract< data_t >::ref( cm_sigma_ )
      ) / fd_extract< data_t >::ref( cm_den_ );
      typename cufft_type< data_t >::complex_type phase_factor;
      cuda_math< data_t >::sin_cos( column * column * aux,
                                    &( phase_factor.y ),
                                    &( phase_factor.x )
                                  );

      typename cufft_type< data_t >::complex_type el = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
      typename cufft_type< data_t >::complex_type retval = {
         el.x * phase_factor.x - el.y * phase_factor.y,
         el.y * phase_factor.x + el.x * phase_factor.y
      };

      return retval;
   }

   __device__ cufftCallbackLoadC d_CZCB_phase_factors_conjugate_multiply_float_ptr  = CZCB_phase_factors_conjugate_multiply< float >;
   __device__ cufftCallbackLoadZ d_CZCB_phase_factors_conjugate_multiply_double_ptr = CZCB_phase_factors_conjugate_multiply< double >;

   template < class data_t >
   struct callback_setter {
   };
   template <>
   struct callback_setter< float > {
      static void set_shift_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_shift_factors_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_shift_factors_conjugate_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_shift_factors_conjugate_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_chirp_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_chirp_factors_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_chirp_factors_conjugate_multiply( cufftHandle plan )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_chirp_factors_conjugate_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                0
                              )
         );
         cuda_sync_check();
      }
      static void set_phase_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_phase_factors_multiply_float_ptr,
                               sizeof( h_ptr )
         );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_phase_factors_conjugate_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_phase_factors_conjugate_multiply_float_ptr,
                               sizeof( h_ptr )
         );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
   };
   template <>
   struct callback_setter< double > {
      static void set_shift_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_shift_factors_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_shift_factors_conjugate_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_shift_factors_conjugate_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX_DOUBLE,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_chirp_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_chirp_factors_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_chirp_factors_conjugate_multiply( cufftHandle plan )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_chirp_factors_conjugate_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                0
                              )
         );
         cuda_sync_check();
      }
      static void set_phase_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_phase_factors_multiply_double_ptr,
                               sizeof( h_ptr )
         );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_phase_factors_conjugate_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_CZCB_phase_factors_conjugate_multiply_double_ptr,
                               sizeof( h_ptr )
         );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
   };
} // namespace utils

#endif // #ifndef CUCZT_KERNELS_H
