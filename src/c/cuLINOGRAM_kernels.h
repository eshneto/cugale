#ifndef CULINOGRAM_KERNELS_H
#define CULINOGRAM_KERNELS_H

#include "utils.h"
#include "cuCZT_kernels.h"

namespace utils {

   __constant__ int cm_pp_row_length_;
   __constant__ int cm_pp_m_;
   __constant__ int cm_pp_n_;
   __constant__ int cm_pp_M_;
   __constant__ int cm_pp_N_;
   __constant__ int cm_pp_b_;
   __constant__ float_double cm_pp_R_1_;
   __constant__ float_double cm_pp_sigma_;
   __constant__ void * cm_pp_ptr_;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type vertical_shift_multiply_CB(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // Logical input stride (equal to number of transforms):
      int input_stride = cm_pp_n_ * cm_pp_b_;

      // Compute row number:
      int row = idx / input_stride;

      // Zero pad if outside physical image range:
      if ( row >= cm_pp_m_ )
         return typename cufft_type< data_t >::complex_type{
            static_cast< data_t >( 0.0 ),
            static_cast< data_t >( 0.0 )
         };

      // Compute column number:
      int i_b = idx % input_stride;
      int column = i_b % cm_pp_n_;

      // In-batch index:
      i_b = i_b / cm_pp_n_;

      // Adjust index to physical position in input array:
      idx = i_b * ( cm_pp_m_ * cm_pp_n_ ) + row * cm_pp_n_ + column;

      // Compute shift factor:
      typename utils::cufft_type< data_t >::complex_type p;
      cuda_math< data_t >::sin_cos( row * ( pi< data_t >::twice *
                                            ( fd_extract< data_t >::ref( cm_pp_R_1_ ) / cm_pp_M_ ) +
                                            fd_extract< data_t >::ref( cm_pp_sigma_ )
                                          ),
                                    &( p.y ),
                                    &( p.x )
                                  );

      // Multiply data point by shift factor:
      typename utils::cufft_type< data_t >::complex_type datum = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
      typename utils::cufft_type< data_t >::complex_type retval = {
         datum.x * p.x - datum.y * p.y,
         datum.y * p.x + datum.x * p.y
      };

      return retval;
   }

   __device__ cufftCallbackLoadC d_LICB_vertical_shift_multiply_float_ptr  = vertical_shift_multiply_CB< float >;
   __device__ cufftCallbackLoadZ d_LICB_vertical_shift_multiply_double_ptr = vertical_shift_multiply_CB< double >;

   template < class data_t >
   __device__ void vertical_store_CB( void * data,
                                      size_t idx,
                                      typename cufft_type< data_t >::complex_type el,
                                      void * callerInfo,
                                      void * sharedPtr
                                    )
   {
      // Logical output stride (equal to number of transforms):
      int output_stride = cm_pp_n_ * cm_pp_b_;

      // Compute line number:
      int row = idx / output_stride;

      // In-batch index:
      int i_b = idx % output_stride;

      // Compute column number:
      int column = i_b % cm_pp_n_;

      // Adjust index to physical position in output array:
      idx = ( i_b / cm_pp_n_ ) * ( cm_pp_M_ * cm_pp_n_ ) + row * cm_pp_n_ + column;

      reinterpret_cast< typename utils::cufft_type< data_t >::complex_type * >( data )[ idx ] = el;
   }

   __device__ cufftCallbackStoreC d_LICB_vertical_store_float_ptr  = vertical_store_CB< float >;
   __device__ cufftCallbackStoreZ d_LICB_vertical_store_double_ptr = vertical_store_CB< double >;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type vertical_load_CB(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // Logical input stride (equal to number of transforms):
      int input_stride = cm_pp_n_ * cm_pp_b_;

      // Compute line number:
      int row = idx / input_stride;

      // Compute column number:
      int i_b = idx % input_stride;
      int column = i_b % cm_pp_n_;

      // In-batch index:
      i_b = i_b / cm_pp_n_;

      // Adjust index to physical position in input array:
      idx = i_b * ( cm_pp_M_ * cm_pp_n_ ) + row * cm_pp_n_ + column;

      return reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
   }

   __device__ cufftCallbackLoadC d_LICB_vertical_load_float_ptr  = vertical_load_CB< float >;
   __device__ cufftCallbackLoadZ d_LICB_vertical_load_double_ptr = vertical_load_CB< double >;

   template < class data_t >
   __device__ void vertical_shift_conjugate_multiply_CB( void * data,
                                                         size_t idx,
                                                         typename cufft_type< data_t >::complex_type el,
                                                         void * callerInfo,
                                                         void * sharedPtr
                                                       )
   {
      // Logical output stride (equal to number of transforms):
      int output_stride = cm_pp_n_ * cm_pp_b_;

      // Compute line number:
      int row = idx / output_stride;

      // Ignore if outside physical image range:
      if ( row >= cm_pp_m_ )
         return;

      // Compute column number:
      int i_b = idx % output_stride;
      int column = i_b % cm_pp_n_;

      // In-batch index:
      i_b = i_b / cm_pp_n_;

      // Adjust index to physical position in output array:
      idx = i_b * ( cm_pp_m_ * cm_pp_n_ ) + row * cm_pp_n_ + column;

      // Compute shift factor:
      typename utils::cufft_type< data_t >::complex_type p;
      cuda_math< data_t >::sin_cos( row * ( pi< data_t >::twice *
                                            ( fd_extract< data_t >::ref( cm_pp_R_1_ ) / cm_pp_M_ ) +
                                            fd_extract< data_t >::ref( cm_pp_sigma_ )
                                          ),
                                    &( p.y ),
                                    &( p.x )
                                  );

      // Multiply data point by shift factor:
      reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_pp_ptr_ )[ idx ] = {
         el.x * p.x + el.y * p.y,
         el.y * p.x - el.x * p.y
      };
   }

   __device__ cufftCallbackStoreC d_LICB_vertical_shift_conjugate_multiply_float_ptr  = vertical_shift_conjugate_multiply_CB< float >;
   __device__ cufftCallbackStoreZ d_LICB_vertical_shift_conjugate_multiply_double_ptr = vertical_shift_conjugate_multiply_CB< double >;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type horizontal_shift_multiply_CB(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // Compute column number:
      int column = idx % cm_pp_M_;

      // Zero pad if outside physical image range:
      if ( column >= cm_pp_n_ )
         return typename cufft_type< data_t >::complex_type{
            static_cast< data_t >( 0.0 ),
            static_cast< data_t >( 0.0 )
         };

      // Compute row number (M_ logical columns per array):
      int row = ( idx / cm_pp_M_ ) % cm_pp_m_;

      // Image number:
      int img = idx / ( cm_pp_m_ * cm_pp_M_ );

      // Adjust index to physical position in input array:
      idx = img * ( cm_pp_m_ * cm_pp_n_ ) + row * cm_pp_n_ + column;

      // Compute shift factor:
      typename utils::cufft_type< data_t >::complex_type p;
      cuda_math< data_t >::sin_cos( column * ( pi< data_t >::twice *
                                               ( fd_extract< data_t >::ref( cm_pp_R_1_ ) / cm_pp_M_ ) +
                                               fd_extract< data_t >::ref( cm_pp_sigma_ )
                                             ),
                                    &( p.y ),
                                    &( p.x )
                                  );

      // Multiply data point by shift factor:
      typename utils::cufft_type< data_t >::complex_type datum = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
      typename utils::cufft_type< data_t >::complex_type retval = {
         datum.x * p.x + datum.y * p.y,
         datum.y * p.x - datum.x * p.y
      };

      return retval;
   }

   __device__ cufftCallbackLoadC d_LICB_horizontal_shift_multiply_float_ptr  = horizontal_shift_multiply_CB< float >;
   __device__ cufftCallbackLoadZ d_LICB_horizontal_shift_multiply_double_ptr = horizontal_shift_multiply_CB< double >;

   template < class data_t >
   __device__ void horizontal_store_CB( void * data,
                                        size_t idx,
                                        typename cufft_type< data_t >::complex_type el,
                                        void * callerInfo,
                                        void * sharedPtr
                                      )
   {
      // Logical output stride (equal to number of transforms):
      int output_stride = cm_pp_m_ * cm_pp_b_;

      // Compute line number:
      size_t row = ( idx / output_stride );

      // In-batch index:
      int i_b = idx % output_stride;

      // Compute column number:
      int column = i_b % cm_pp_m_;

      // Adjust index to physical position in input array:
      idx = ( i_b / cm_pp_m_ ) * ( cm_pp_M_ * cm_pp_m_ ) + row * cm_pp_m_ + column;

      // Compute shift factor:
      reinterpret_cast< typename utils::cufft_type< data_t >::complex_type * >( data )[ idx ] = el;
   }

   __device__ cufftCallbackStoreC d_LICB_horizontal_store_float_ptr  = horizontal_store_CB< float >;
   __device__ cufftCallbackStoreZ d_LICB_horizontal_store_double_ptr = horizontal_store_CB< double >;

   template < class data_t >
   __device__ typename cufft_type< data_t >::complex_type horizontal_load_CB(
      void * data,
      size_t idx,
      void * callerInfo,
      void * sharedPtr
   )
   {
      // Logical input stride (equal to number of transforms):
      int input_stride = cm_pp_m_ * cm_pp_b_;

      // Compute line number:
      int row = idx / input_stride;

      // Compute column number:
      int i_b = idx % input_stride;
      int column = i_b % cm_pp_m_;

      // In-batch index:
      i_b = i_b / cm_pp_m_;

      // Adjust index to physical position in input array:
      idx = i_b * ( cm_pp_M_ * cm_pp_m_ ) + row * cm_pp_m_ + column;

      return reinterpret_cast< typename cufft_type< data_t >::complex_type * >( data )[ idx ];
   }

   __device__ cufftCallbackLoadC d_LICB_horizontal_load_float_ptr  = horizontal_load_CB< float >;
   __device__ cufftCallbackLoadZ d_LICB_horizontal_load_double_ptr = horizontal_load_CB< double >;

   template < class data_t >
   __device__ void horizontal_shift_conjugate_multiply_sum_CB( void * data,
                                                               size_t idx,
                                                               typename cufft_type< data_t >::complex_type el,
                                                               void * callerInfo,
                                                               void * sharedPtr
                                                             )
   {
      // Compute column number:
      int column = idx % cm_pp_M_;

      // Ignore if outside physical image range:
      if ( column >= cm_pp_n_ )
         return;

      // Compute line number:
      int i_b = idx / cm_pp_M_;
      int row = i_b % cm_pp_m_;

      // In-batch index:
      i_b = i_b / cm_pp_m_;

      // Adjust index to physical position in output array:
      idx = i_b * ( cm_pp_m_ * cm_pp_n_ ) + row * cm_pp_n_ + column;

      // Compute shift factor:
      typename utils::cufft_type< data_t >::complex_type p;
      cuda_math< data_t >::sin_cos( column * ( pi< data_t >::twice *
                                               ( fd_extract< data_t >::ref( cm_pp_R_1_ ) / cm_pp_M_ ) +
                                               fd_extract< data_t >::ref( cm_pp_sigma_ )
                                             ),
                                    &( p.y ),
                                    &( p.x )
                                  );

      // Multiply data point by shift factor:
      typename cufft_type< data_t >::complex_type datum = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_pp_ptr_ )[ idx ];
      reinterpret_cast< typename cufft_type< data_t >::complex_type * >( cm_pp_ptr_ )[ idx ] = {
         datum.x + el.x * p.x - el.y * p.y,
         datum.y + el.y * p.x + el.x * p.y
      };
   }

   __device__ cufftCallbackStoreC d_LICB_horizontal_shift_conjugate_multiply_sum_float_ptr  = horizontal_shift_conjugate_multiply_sum_CB< float >;
   __device__ cufftCallbackStoreZ d_LICB_horizontal_shift_conjugate_multiply_sum_double_ptr = horizontal_shift_conjugate_multiply_sum_CB< double >;

   template < class data_t >
   struct li_callback_setter {
   };
   template <>
   struct li_callback_setter< float > {
      static void set_vertical_shift_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_shift_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_vertical_store( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_store_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_vertical_load( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_load_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_vertical_shift_factors_conjugate_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_shift_conjugate_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_store( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_store_float_ptr,
                               sizeof( h_ptr )
         );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_shift_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_shift_multiply_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_load( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_load_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_shift_factors_conjugate_multiply_sum( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreC h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_shift_conjugate_multiply_sum_float_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX,
                                &arg
            )
         );
         cuda_sync_check();
      }
   };
   template <>
   struct li_callback_setter< double > {
      static void set_vertical_shift_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_shift_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
                              )
         );
         cuda_sync_check();
      }
      static void set_vertical_store( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_store_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_vertical_load( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_load_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_vertical_shift_factors_conjugate_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_vertical_shift_conjugate_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_store( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_store_double_ptr,
                               sizeof( h_ptr )
         );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_shift_factors_multiply( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_shift_multiply_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_load( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackLoadZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_load_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_LD_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
      static void set_horizontal_shift_factors_conjugate_multiply_sum( cufftHandle plan, void * arg = 0 )
      {
         cufftCallbackStoreZ h_ptr;
         cudaMemcpyFromSymbol( &h_ptr,
                               d_LICB_horizontal_shift_conjugate_multiply_sum_double_ptr,
                               sizeof( h_ptr )
                             );
         cuda_sync_check();
         cufft_check(
            cufftXtSetCallback( plan,
                                ( void ** ) &h_ptr,
                                CUFFT_CB_ST_COMPLEX_DOUBLE,
                                &arg
            )
         );
         cuda_sync_check();
      }
   };

   template < class data_t >
   void linogram_to_constant_memory( int row_length, int m, int n, int M, int b, data_t R, data_t sigma, void * ptr = 0 )
   {
      cudaMemcpyToSymbol( cm_pp_row_length_, &row_length, sizeof( row_length ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_m_, &m, sizeof( m ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_n_, &n, sizeof( n ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_M_, &M, sizeof( M ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_b_, &b, sizeof( b ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_R_1_, &R, sizeof( R ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_sigma_, &sigma, sizeof( sigma ) );
      utils::cuda_sync_check();
      cudaMemcpyToSymbol( cm_pp_ptr_, &ptr, sizeof( ptr ) );
      utils::cuda_sync_check();
   }
} // namespace utils

#endif // #ifndef CULINOGRAM_KERNELS_H
