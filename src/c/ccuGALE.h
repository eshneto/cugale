#ifndef CCUGALE_H

extern "C" {

   void * cuGALE_create( int m,
                         int n,
                         int b,
                         int M,
                         int N,
                         double const * angles,
                         int S,
                         int N_L,
                         double sigma_h,
                         double sigma_v,
                         double epsilon
                       );
   void cuGALE_destroy( void * obj );
   void cuGALE_forward( void * obj, void * in, void * out, void * buffer );
   void cuGALE_adjoint( void * obj, void * in, void * out, void * buffer );

}

#define CCUGALE_H
#endif // #ifndef CCUGALE_H
