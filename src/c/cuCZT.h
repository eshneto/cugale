#ifndef CUCZT_H
#define CUCZT_H

#include "cuCZT_kernels.h"
#include "utils.h"
#include <cufftXt.h>
#include <exception>
#include <complex>

/**
 * \file This file contains definitions of a class template that computes a series of chirp-z
 * transforms.
 */

template < class data_t >
using cuCZT_data = utils::cuCZT_data< data_t >;

template < class data_t >
struct cuCZT : public cuCZT_data< data_t > {

   using cuCZT_data< data_t >::m_;
   using cuCZT_data< data_t >::M_;
   using cuCZT_data< data_t >::P_;
   using cuCZT_data< data_t >::E_;
   using cuCZT_data< data_t >::b_;
   using cuCZT_data< data_t >::row_length_;
   using cuCZT_data< data_t >::plane_area_;
   using cuCZT_data< data_t >::R_1_;
   using cuCZT_data< data_t >::R_2_;
   using cuCZT_data< data_t >::sigma_;
   using cuCZT_data< data_t >::den_;
   using cuCZT_data< data_t >::chirp_factors_;
   using cuCZT_data< data_t >::shift_factors_;

   cuCZT_data< data_t > * constant_mem_obj_; ///< Store for object data in constant memory.

   typedef data_t data_type; ///< Floating point type.
   static cufftType_t const transform_type = utils::cufft_type< data_type >::transform_type; ///< Type of cuFFT transform.

   std::complex< data_type > * in_;     ///< Input array.
   std::complex< data_type > * out_;    ///< Output array.
   std::complex< data_type > * buffer_; ///< Buffer for intermediate FFT output.
   void * cufft_workspace_;             ///< cuFFT's workspace.

   cufftHandle forward_plan_;
   cufftHandle backward_plan_;
   cufftHandle adjoint_forward_plan_;
   cufftHandle adjoint_backward_plan_;

   cuCZT( int m,
          int M,
          int P,
          int E,
          int b,
          data_type R_1,
          data_type R_2,
          data_type sigma,
          std::complex< data_type > * in     = 0,
          std::complex< data_type > * out    = 0,
          std::complex< data_type > * buffer = 0,
          cuCZT_data< data_t > * constant_mem_obj = 0
        );
   ~cuCZT();

   void forward( std::complex< data_type > * in = 0, std::complex< data_type > * out = 0, std::complex< data_type > * buffer = 0 );
   void adjoint( std::complex< data_type > * in = 0, std::complex< data_type > * out = 0, std::complex< data_type > * buffer = 0 );

   void self_to_constant_memory( void * ptr = 0 );
};

/**
 * \param m      Input length \f[ m \f] (number of image columns).
 * \param M      Number of inputs \f[ M \f] (number of samples per spoke).
 * \param P      Output length (\f[ P = N_L / 2 + 2( S + 1 ) \f], where \f[ N_L \f] is the total number
 *               of linogram rays and \f[ S \f] is the number terms in truncated sum).
 * \param E      Number of "extra" elements in the CZT. Should be \f[2( S + 1 )\f].
 * \param b      Batch size \f[ b \f].
 * \param R_1    Shift \f[ R_1 \f] in \f[ I \f] index: \f[ I \in \{ -R_1, -R_1 + 1, \dots, -R_1 + M - 1 \} \f].
 * \param R_2    Shift \f[ R_2 \f] in \f[ J \f] index: \f[ J \in \{ -R_2, -R_2 + 1, \dots, -R_2 + P - 1 \} \f].
 * \param sigma  Shift \f[ \sigma \f] in k-space for each ray.
 * \param in     Input array. Should be of dimensions \f[ 2P \times M \times b \f]. It should be organized in lines
 *               of length \f[ 2P \f] where the last \f[ 2P - m \f] elements of each line are zero.
 * \param out    Output array. Should be of dimensions \f[ 2P \times M \times b \f]. It should be organized in lines
 *               of length \f[ 2P \f] where the last \f[ P \f] elements should be ignored.
 * \param buffer Intermediate array. Should be of dimensions \f[ 2P \times M \times b \f].
 */
template < class data_t >
cuCZT< data_t >::cuCZT( int m,
                        int M,
                        int P,
                        int E,
                        int b,
                        data_type R_1,
                        data_type R_2,
                        data_type sigma,
                        std::complex< data_type > * in,
                        std::complex< data_type > * out,
                        std::complex< data_type > * buffer,
                        cuCZT_data< data_t > * constant_mem_obj
                      )
   : cuCZT_data< data_t >( m, M, P, E, b, R_1, R_2, sigma ),
     in_( in ), out_( out ), buffer_( buffer ), constant_mem_obj_( constant_mem_obj )
{
   // We will use separate plans for the various FFTs because callbacks
   // can be set only once for each plan.
   // Therefore, we will manage the workspace for the transforms in
   // order to avoid excessive memory usage.

   // Create plans:
   utils::cufft_check(
      cufftCreate( &forward_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( forward_plan_, 0 )
   );
   size_t forward_plan_size;
   utils::cufft_check(
      // Creates a plan for M*b 1D FFTs of length 2*P:
      cufftMakePlanMany( forward_plan_,  ///< Plan.
                         1,              ///< Transforms rank.
                         &row_length_,   ///< Individual transforms dimensions.
                         NULL,           ///< Input embedding.
                         1,              ///< Input stride.
                         row_length_,    ///< Input distance.
                         NULL,           ///< Output embedding.
                         1,              ///< Output stride.
                         row_length_,    ///< Output distance.
                         transform_type, ///< Transform type.
                         M_ * b_,        ///< Batch size.
                         &forward_plan_size
                       )
   );

   utils::cufft_check(
      cufftCreate( &backward_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( backward_plan_, 0 )
   );
   size_t backward_plan_size;
   utils::cufft_check(
      // Creates a plan for M*b 1D FFTs of length 2*P:
      cufftMakePlanMany( backward_plan_,  ///< Plan.
                         1,               ///< Transforms rank.
                         &row_length_,     ///< Individual transforms dimensions.
                         NULL,            ///< Input embedding.
                         1,               ///< Input stride.
                         row_length_,      ///< Input distance.
                         NULL,            ///< Output embedding.
                         1,               ///< Output stride.
                         row_length_,      ///< Output distance.
                         transform_type,  ///< Transform type.
                         M_ * b_,         ///< Batch size.
                         &backward_plan_size
                       )
   );

   utils::cufft_check(
      cufftCreate( &adjoint_forward_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( adjoint_forward_plan_, 0 )
   );
   size_t adjoint_forward_plan_size;
   utils::cufft_check(
      // Creates a plan for M*b 1D FFTs of length 2*P:
      cufftMakePlanMany( adjoint_forward_plan_,  ///< Plan.
                         1,               ///< Transforms rank.
                         &row_length_,     ///< Individual transforms dimensions.
                         NULL,            ///< Input embedding.
                         1,               ///< Input stride.
                         row_length_,      ///< Input distance.
                         NULL,            ///< Output embedding.
                         1,               ///< Output stride.
                         row_length_,      ///< Output distance.
                         transform_type,  ///< Transform type.
                         M_ * b_,         ///< Batch size.
                         &adjoint_forward_plan_size
                       )
   );

   utils::cufft_check(
      cufftCreate( &adjoint_backward_plan_ )
   );
   utils::cufft_check(
      cufftSetAutoAllocation( adjoint_backward_plan_, 0 )
   );
   size_t adjoint_backward_plan_size;
   utils::cufft_check(
      // Creates a plan for M*b 1D FFTs of length 2*P:
      cufftMakePlanMany( adjoint_backward_plan_,  ///< Plan.
                         1,               ///< Transforms rank.
                         &row_length_,    ///< Individual transforms dimensions.
                         NULL,            ///< Input embedding.
                         1,               ///< Input stride.
                         row_length_,     ///< Input distance.
                         NULL,            ///< Output embedding.
                         1,               ///< Output stride.
                         row_length_,     ///< Output distance.
                         transform_type,  ///< Transform type.
                         M_ * b_,         ///< Batch size.
                         &adjoint_backward_plan_size
                       )
   );

   // Compute needed workspace size
   size_t plan_size = std::max( std::max( forward_plan_size, backward_plan_size ), std::max( adjoint_forward_plan_size, adjoint_backward_plan_size ) );

   // Allocate workspace
   cudaMalloc( & cufft_workspace_, plan_size );
   utils::cuda_sync_check();

   // Set workspace:
   utils::cufft_check(
      cufftSetWorkArea( forward_plan_, cufft_workspace_ )
   );
   utils::cufft_check(
      cufftSetWorkArea( backward_plan_, cufft_workspace_ )
   );
   utils::cufft_check(
      cufftSetWorkArea( adjoint_forward_plan_, cufft_workspace_ )
   );
   utils::cufft_check(
      cufftSetWorkArea( adjoint_backward_plan_, cufft_workspace_ )
   );

   // Allocate memory for shift factors:
   cudaMalloc( ( void * * ) & shift_factors_, sizeof( std::complex< data_type > ) * m_ * M_ );
   utils::cuda_sync_check();
   // Compute shift factors:
   for ( int i = 0; i < m_ / 1024; ++i )
   {
      int i_0 = i * 1024;
      utils::shift_factors_compute<<< M_, 1024 >>>( shift_factors_, M_, m_, R_1_, R_2_, sigma_, den_, i_0 );
      utils::cuda_sync_check();
   }
   if ( m_ % 1024 )
   {
      int i_0 = ( m_ / 1024 ) * 1024;
      utils::shift_factors_compute<<< M_, m_ % 1024 >>>( shift_factors_, M_, m_, R_1_, R_2_, sigma_, den_, i_0 );
      utils::cuda_sync_check();
   }

   // Allocate memory for chirp factors:
   cudaMalloc( ( void * * ) & chirp_factors_, sizeof( std::complex< data_type > ) * row_length_ * M_ );
   utils::cuda_sync_check();
   // Compute chirp factors. Code has to be run multiple times if P_ is greater than 1024.
   for ( int i = 0; i < P_ / 1024; ++i )
   {
      int i_0 = i * 1024;
      utils::chirp_factors_compute_first_half<<< M_, 1024 >>>( chirp_factors_, M_, row_length_, R_1_, sigma_, den_, i_0 );
      utils::cuda_sync_check();
      utils::chirp_factors_compute_second_half<<< M_, 1024 >>>( chirp_factors_, M_, P_, row_length_, R_1_, sigma_, den_, i_0 );
      utils::cuda_sync_check();
   }
   if ( P_ % 1024 )
   {
      int i_0 = ( P_ / 1024 ) * 1024;
      utils::chirp_factors_compute_first_half<<< M_, P_ % 1024 >>>( chirp_factors_, M_, row_length_, R_1_, sigma_, den_, i_0 );
      utils::cuda_sync_check();
      utils::chirp_factors_compute_second_half<<< M_, P_ % 1024 >>>( chirp_factors_, M_, P_, row_length_, R_1_, sigma_, den_, i_0 );
      utils::cuda_sync_check();
   }

   // FFT of the chirp_factors:
   cufftHandle chirp_plan;
   utils::cufft_check(
      // Creates a plan for M*b 1D FFTs of length 2*P:
      cufftPlanMany( &chirp_plan,     ///< Plan.
                     1,               ///< Transforms rank.
                     &row_length_,    ///< Individual transforms dimensions.
                     NULL,            ///< Input embedding.
                     1,               ///< Input stride.
                     row_length_,     ///< Input distance.
                     NULL,            ///< Output embedding.
                     1,               ///< Output stride.
                     row_length_,     ///< Output distance.
                     transform_type,  ///< Transform type.
                     M_               ///< Batch size.
                   )
   );
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( chirp_plan, chirp_factors_, chirp_factors_, CUFFT_FORWARD )
   );
   utils::cufft_check(
      cufftDestroy( chirp_plan )
   );

   // Set corresponding callbacks:
   utils::callback_setter< data_type >::set_shift_factors_multiply( forward_plan_ );
   utils::callback_setter< data_type >::set_chirp_factors_multiply( backward_plan_ );
   utils::callback_setter< data_type >::set_phase_factors_multiply( backward_plan_ );
   utils::callback_setter< data_type >::set_phase_factors_conjugate_multiply( adjoint_forward_plan_ );
   utils::callback_setter< data_type >::set_chirp_factors_conjugate_multiply( adjoint_backward_plan_ );
   utils::callback_setter< data_type >::set_shift_factors_conjugate_multiply( adjoint_backward_plan_ );
}

template < class data_t >
void cuCZT< data_t >::forward( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer )
{
   // Parameter "in" is assumed to have M_ * b_ rows each of length m_.
   // CZT of each row is returned in corresponding row of "out", which is assumed to have
   // M_ * b_ rows of length 2 * P_, but only the first P_ elements of each row are used.
   // "buffer" must be of size 2 * P_ * M_ * b_.
   //
   // Each parameter can be set to NULL (the default value), in which case the storage pointed to by
   // in_, buffer_, and out_ are respectively used for input, buffering, and output.

   // Switch to internal memory as required:
   if ( in == NULL )
      in = in_;
   if ( out == NULL )
      out = out_;
   if ( buffer == NULL )
      buffer = buffer_;

   // Set constant memory variables:
   self_to_constant_memory( out );

   // Run DFT of the data.
   // Pre-multiplication by shift factors executed in load callback.
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( forward_plan_, in, buffer, CUFFT_FORWARD )
   );
   utils::cuda_sync_check();

   // Run adjoint DFT of the multiplied DFT result:
   // Pre-multiplication by chirp factors executed in load callback.
   // Post-multiplication by phase factors executed in store callback.
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( backward_plan_, buffer, buffer, CUFFT_INVERSE )
   );
   utils::cuda_sync_check();
}

template < class data_t >
void cuCZT< data_t >::adjoint( std::complex< data_type > * in, std::complex< data_type > * out, std::complex< data_type > * buffer )
{
   // Parameter "in" is assumed to have M_ * b_ rows each of length 2 * P_. Of each row, only P_ elements
   // will be actually accessed. The remaining are assumed to be 0.
   // This allows properly splitting horizontal and vertical butterflies in linogram output.
   // CZT of each row is returned in corresponding row of "out", which is assumed to have
   // M_ * b_ rows of length m.
   // "buffer" must be of size 2 * P_ * M_ * b_.
   //
   // Each parameter can be set to NULL (the default value), in which case the storage pointed to by
   // in_, buffer_, and out_ are respectively used for input, buffering, and output.

   // Switch to internal memory as required:
   if ( in == NULL )
      in = in_;
   if ( out == NULL )
      out = out_;
   if ( buffer == NULL )
      buffer = buffer_;

   // Set constant memory variables:
   self_to_constant_memory( out );

   // Run DFT of the multiplied DFT result.
   // Pre-multiplication by conjugate phase factors executed in load callback.
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( adjoint_forward_plan_, in, buffer, CUFFT_FORWARD )
   );
   utils::cuda_sync_check();

   // Run adjoint DFT of the data.
   // Pre-multiplication by conjugate chirp factors executed in load callback.
   // Post-multiplication by conjugate shift factors executed in store callback.
   utils::cufft_check(
      utils::cufft_type< data_type >::execute( adjoint_backward_plan_, buffer, buffer, CUFFT_INVERSE )
   );
   utils::cuda_sync_check();
}

template < class data_t >
void cuCZT< data_t >::self_to_constant_memory( void * ptr )
{
   cudaMemcpyToSymbol( utils::cm_chirp_factors_, &chirp_factors_, sizeof( chirp_factors_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_shift_factors_, &shift_factors_, sizeof( shift_factors_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_plane_area_, &plane_area_, sizeof( plane_area_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_row_length_, &row_length_, sizeof( row_length_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_m_, &m_, sizeof( m_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_M_, &M_, sizeof( M_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_P_, &P_, sizeof( P_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_R_1_, &R_1_, sizeof( R_1_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_R_2_, &R_2_, sizeof( R_2_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_den_, &den_, sizeof( den_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_sigma_, &sigma_, sizeof( sigma_ ) );
   utils::cuda_sync_check();
   cudaMemcpyToSymbol( utils::cm_ptr_, &ptr, sizeof( ptr ) );
   utils::cuda_sync_check();
}

template < class data_t >
cuCZT< data_t >::~cuCZT()
{
   cudaFree( chirp_factors_ );
   utils::cuda_sync_check();
   cudaFree( shift_factors_ );
   utils::cuda_sync_check();
   cudaFree( cufft_workspace_ );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( forward_plan_ )
   );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( backward_plan_ )
   );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( adjoint_forward_plan_ )
   );
   utils::cuda_sync_check();
   utils::cufft_check(
      cufftDestroy( adjoint_backward_plan_ )
   );
   utils::cuda_sync_check();
}

#endif // #ifndef CUCZT_H
