#ifndef CUGALE_H
#define CUGALE_H

#include "cuLINOGRAM.h"
#include "cuGALE_kernels.h"
#include "cuda_array.h"

#include <thrust/device_ptr.h>
#include <thrust/fill.h>

#ifndef BLSZ
#define BLSZ 1024
#endif

template < class data_t >
struct cuGALE {

   typedef data_t data_type; ///< Floating point type.

   std::vector< std::complex< data_t > > linear_coefficients_; ///< Coefficients for linear combinations to obtain approximations.
   std::vector< std::vector< int > > combination_indices_; ///< Indices of values to be used in linear combinations.

   cuda_array< std::complex< data_t > > weights_;
   cuda_array< int > indices_;

   cuLINOGRAM< data_type > lino_obj_; ///< Linogram object.

   int b_; ///< Batch size
   int M_; ///< Number of lines
   int S_;
   int N_;
   int N_L_;

   cuGALE ( int m,
            int n,
            int b,
            int M,
            int N,
            double const * angles,
            int S,
            int N_L,
            double sigma_h,
            double sigma_v,
            double epsilon = data_type( 1.0 ) - data_type( 1e-4 )
   );

   void forward( std::complex< data_t > * in,
                 std::complex< data_t > * out,
                 std::complex< data_t > * buffer
               );
   void adjoint( std::complex< data_t > * in,
                 std::complex< data_t > * out,
                 std::complex< data_t > * buffer
               );
};

template < class data_t >
cuGALE< data_t >::cuGALE ( int m,
                           int n,
                           int b,
                           int M,
                           int N,
                           double const * angles,
                           int S,
                           int N_L,
                           double sigma_h,
                           double sigma_v,
                           double epsilon
                         )
   : lino_obj_( m, n, M, N_L, S, b, sigma_h, sigma_v ),
     linear_coefficients_( N * ( 2 * S + 1 ) * M ),
     weights_( N * ( 2 * S + 1 ) * M ),
     indices_( N ),
     b_( b ), M_( M ), S_( S ), N_( N ), N_L_( N_L )
{
   // "Horizontal" initialization:
   for ( int i = 0; i < m / 1024; ++i )
   {
      int i_0 = i * 1024;
      utils::shift_factors_adjust< data_t ><<< M, 1024 >>>( lino_obj_.czt_obj_horizontal_.shift_factors_, M, m, N_L, S, M / 2, -sigma_h, epsilon, i_0 );
      utils::cuda_sync_check();
   }
   if ( m % 1024 )
   {
      int i_0 = ( m / 1024 ) * 1024;
      utils::shift_factors_adjust< data_t ><<< M, m % 1024 >>>( lino_obj_.czt_obj_horizontal_.shift_factors_, M, m, N_L, S, M / 2, -sigma_h, epsilon, i_0 );
      utils::cuda_sync_check();
   }
   // "Vertical" initialization:
   for ( int i = 0; i < n / 1024; ++i )
   {
      int i_0 = i * 1024;
      utils::shift_factors_adjust< data_t ><<< M, 1024 >>>( lino_obj_.czt_obj_vertical_.shift_factors_, M, n, N_L, S, M / 2, -sigma_v, epsilon, i_0 );
      utils::cuda_sync_check();
   }
   if ( n % 1024 )
   {
      int i_0 = ( n / 1024 ) * 1024;
      utils::shift_factors_adjust< data_t ><<< M, n % 1024 >>>( lino_obj_.czt_obj_vertical_.shift_factors_, M, n, N_L, S, M / 2, -sigma_v, epsilon, i_0 );
      utils::cuda_sync_check();
   }

   // Compute parameters of linear combinations:
   std::vector< std::complex< double > > linear_coefficients_double( linear_coefficients_.size() );
   utils::create_linear_combinations( M, N_L, ( S + 1 ) * 4, S,
                                      m, n,
                                      angles, N,
                                      N_L / 4 - 1, N_L / 4,
                                      M / 2, M / 2 - 1,
                                      linear_coefficients_double, combination_indices_,
                                      sigma_h, sigma_v, epsilon
                                    );
   for ( int i = 0; i < linear_coefficients_.size(); ++i )
      linear_coefficients_[ i ] = linear_coefficients_double[ i ];

   // Upload parameters of linear combinations to GPU memory:
   weights_.host_data_ = &( linear_coefficients_[ 0 ] );
   weights_.device_alloc();
   weights_.host_device();
   weights_.host_data_ = 0;

   std::vector< int > indices( N );
   for ( int i = 0; i < N; ++i )
      indices[ i ] = combination_indices_[ i ][ 0 ];

   indices_.host_data_ = &( indices[ 0 ] );
   indices_.device_alloc();
   indices_.host_device();
   indices_.host_data_ = 0;
}

template < class data_t >
void cuGALE< data_t >::forward( std::complex< data_t > * in,
                                std::complex< data_t > * out,
                                std::complex< data_t > * buffer
                              )
{
   lino_obj_.forward( in, buffer, lino_obj_.cuczt_workspace_ );
   dim3 grid( M_, b_ );
   for ( int i = 0; i < N_ / BLSZ; ++i )
   {
      int N_0 = i * BLSZ;
      utils::truncated_sum<<< grid, BLSZ >>>( 2 * S_ + 1, M_, N_, N_L_ + 4 * ( S_ + 1 ),
                                              indices_.device_data_,
                                              weights_.device_data_,
                                              buffer,
                                              out,
                                              N_0
                                            );
   }
   if ( N_ % BLSZ )
      utils::truncated_sum<<< grid, N_ % BLSZ >>>( 2 * S_ + 1, M_, N_, N_L_ + 4 * ( S_ + 1 ),
                                                   indices_.device_data_,
                                                   weights_.device_data_,
                                                   buffer,
                                                   out,
                                                   ( N_ / BLSZ ) * BLSZ
                                                 );
   utils::cuda_sync_check();
}

template < class data_t >
void cuGALE< data_t >::adjoint( std::complex< data_t > * in,
                                std::complex< data_t > * out,
                                std::complex< data_t > * buffer
                              )
{
   thrust::device_ptr< data_t > dev_ptr( reinterpret_cast< data_t * >( buffer ) );
   thrust::fill( dev_ptr, dev_ptr + 2 * ( M_ * ( N_L_ + 4 * ( S_ + 1 ) ) * b_ ), static_cast< data_t >( 0.0 ) );
   utils::cuda_sync_check();

   dim3 grid( M_, b_ );
   for ( int i = 0; i < N_ / BLSZ; ++i )
   {
      int N_0 = i * BLSZ;
      utils::adjoint_truncated_sum<<< grid, BLSZ >>>( 2 * S_ + 1, M_, N_, N_L_ + 4 * ( S_ + 1 ),
                                                      indices_.device_data_,
                                                      weights_.device_data_,
                                                      in,
                                                      buffer,
                                                      N_0
                                                    );
   }
   if ( N_ % BLSZ )
      utils::adjoint_truncated_sum<<< grid, N_ % BLSZ >>>( 2 * S_ + 1, M_, N_, N_L_ + 4 * ( S_ + 1 ),
                                                           indices_.device_data_,
                                                           weights_.device_data_,
                                                           in,
                                                           buffer,
                                                           ( N_ / BLSZ ) * BLSZ
                                                         );
   utils::cuda_sync_check();

   lino_obj_.adjoint( buffer, out, lino_obj_.cuczt_workspace_ );
}

#endif // #ifndef CUGALE_H
