//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef DFT_H
#define DFT_H

#include <complex>
#include <thread>
#include <vector>
#include <cmath>
#include <algorithm>

/**
 * \file dft.h
 *
 * This file contains functions to compute the Discrete Fourier Transform by brute force, i.e., by direct summation of the formula
 * \f[
 *    X( \xi, \upsilon ) := \sum_{i = 0}^{m - 1}\sum_{j = 0}^{n - 1}x_{i, j}e^{-\imath( j\xi + i\upsilon )}.
 * \f]
 */

/**
 * Namespace for all dft related stuff.
 */
namespace dft {

   double DPI  = 6.2831853071795864769;    ///< \f$2\pi\f$.
   double TQPI = 2.3561944901923449288;    ///< \f$3\pi/4\f$.
   double PI   = 3.1415926535897932385;    ///< \f$\pi\f$.
   double HPI  = 1.5707963267948966192;    ///< \f$\pi/2\f$.
   double QPI  = 7.8539816339744830962e-1; ///< \f$\pi/4\f$.
   double GA   = 1.9416110387254665773;    ///< Golden angle in radians: \f$2\pi/(1 + \sqrt 5)\f$.

   /**
    * Compute select lines of the DFT of a discrete complex 2D image at given samples using direct summation.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param m Number of data lines.
    * \param n Number of data columns.
    * \param data Data.
    * \param M Number of sample lines.
    * \param N Number of sample columns.
    * \param xi Horizontal coordinates of samples.
    * \param upsilon Vertical coordinates of samples.
    * \param result Array or samples storage.
    * \param thread_id Current working thread.
    * \param n_threads Number of working threads.
    *
    */
   template < class d_type >
   void dft_worker_complex_data(
                                 int m, int n,
                                 std::complex< d_type > const * data,
                                 int M, int N,
                                 d_type const * xi,
                                 d_type const * upsilon,
                                 std::complex< d_type > * result,
                                 int thread_id,
                                 int n_threads = 0
                              )
   {
      for ( int i = thread_id; i < M; i += n_threads )
         for ( int j = 0; j < N; ++j )
         {
            d_type curr_xi = xi[ j + i * N ];
            d_type curr_upsilon = upsilon[ j + i * N ];
            std::complex< d_type > & curr_sample = result[ j + i * N ];
            curr_sample = 0.0;
            for ( int mu = 0; mu < m; ++mu )
               for ( int nu = 0; nu < n; ++nu )
               {
                  d_type arg = nu * curr_xi + mu * curr_upsilon;
                  curr_sample += data[ nu + mu * n ] * std::complex< d_type >( std::cos( arg ), -std::sin( arg ) );
               }
         }
   }

   /**
    * Compute select lines of the DFT of a discrete real 2D image at given samples using direct summation.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param m Number of data lines.
    * \param n Number of data columns.
    * \param data Data.
    * \param M Number of sample lines.
    * \param N Number of sample columns.
    * \param xi Horizontal coordinates of samples.
    * \param upsilon Vertical coordinates of samples.
    * \param result Array or samples storage.
    * \param thread_id Current working thread.
    * \param n_threads Number of working threads.
    *
    */
   template < class d_type >
   void dft_worker_real_data(
                              int m, int n,
                              d_type const * data,
                              int M, int N,
                              d_type const * xi,
                              d_type const * upsilon,
                              std::complex< d_type > * result,
                              int thread_id,
                              int n_threads = 0
                           )
   {
      for ( int i = thread_id; i < M; i += n_threads )
         for ( int j = 0; j < N; ++j )
         {
            d_type curr_xi = xi[ j + i * N ];
            d_type curr_upsilon = upsilon[ j + i * N ];
            std::complex< d_type > & curr_sample = result[ j + i * N ];
            curr_sample = 0.0;
            for ( int mu = 0; mu < m; ++mu )
               for ( int nu = 0; nu < n; ++nu )
               {
                  d_type arg = nu * curr_xi + mu * curr_upsilon;
                  curr_sample += data[ nu + mu * n ] * std::complex< d_type >( std::cos( arg ), -std::sin( arg ) );
               }
         }
   }

   /**
   * Compute the DFT of a discrete complex 2D image at given samples using direct summation.
   *
   * \tparam d_type Type of numerical data, can be any floating point type.
   * \param m Number of data lines.
   * \param n Number of data columns.
   * \param data Data.
   * \param M Number of sample lines.
   * \param N Number of sample columns.
   * \param xi Horizontal coordinates of samples.
   * \param upsilon Vertical coordinates of samples.
   * \param result Array or samples storage.
   * \param n_threads Number of working threads.
   */
   template < class d_type >
   void dft(
            int m, int n,
            std::complex< d_type > const * data,
            int M, int N,
            d_type const * xi,
            d_type const * upsilon,
            std::complex< d_type > * result,
            int n_threads = 0
           )
   {
      if ( !n_threads )
         n_threads = std::thread::hardware_concurrency();

      std::vector< std::thread > v_threads;
      v_threads.reserve( n_threads );

      for ( int i = 0; i < n_threads; ++i )
         v_threads.push_back( std::thread( dft_worker_complex_data< d_type >, m, n, data, M, N, xi, upsilon, result, i, n_threads ) );

      for ( int i = 0; i < n_threads; ++i )
         v_threads[ i ].join();
   }
   /**
    * Compute the DFT of a discrete real 2D image at given samples using direct summation.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param m Number of data lines.
    * \param n Number of data columns.
    * \param data Data.
    * \param M Number of sample lines.
    * \param N Number of sample columns.
    * \param xi Horizontal coordinates of samples.
    * \param upsilon Vertical coordinates of samples.
    * \param result Array or samples storage.
    * \param n_threads Number of working threads.
    */
   template < class d_type >
   void dft(
            int m, int n,
            d_type const * data,
            int M, int N,
            d_type const * xi,
            d_type const * upsilon,
            std::complex< d_type > * result,
            int n_threads = 0
           )
   {
      if ( !n_threads )
         n_threads = std::thread::hardware_concurrency();

      std::vector< std::thread > v_threads;
      v_threads.reserve( n_threads );

      for ( int i = 0; i < n_threads; ++i )
         v_threads.push_back( std::thread( dft_worker_real_data< d_type >, m, n, data, M, N, xi, upsilon, result, i, n_threads ) );

      for ( int i = 0; i < n_threads; ++i )
         v_threads[ i ].join();
   }

   /**
    * \brief Small golden angles.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param n Number of angles.
    * \param first_angle First angle to be computed.
    *
    * This function computes the golden-angle (spaced \f$2\pi/( 1 + \sqrt 5 )\f$ apart from each other) sequence, modulo \f$\pi\f$, in increasing order and in the interval \f$( \pi / 4, 5\pi / 4 ]\f$.
    */
   template< class d_type = double >
   std::vector< d_type > golden_angles( int n, d_type first_angle = 0.0 )
   {
      // Vector for storing the angles:
      std::vector< d_type > retval;
      retval.reserve( n );

      // Compute sequence of angles:
      retval.push_back( first_angle );
      for( int i = 1; i < n; ++i )
         retval.push_back( retval[ i - 1 ] + GA );

      // Constrain, modulo \f$\pi\f$, to the interval \f$( \pi / 4, 5\pi / 4 ]\f$
      for( int i = 0; i < n; ++i )
      {
         d_type angle = retval[ i ] - QPI;
         int mul = angle / PI;
         angle -= PI * mul;
         angle = ( angle <= 0.0 ) ? angle + PI : angle;
         retval[ i ] = angle + QPI;
      }

      // Sort angles:
      std::sort( retval.begin(), retval.end() );

      return retval;
   }

   /**
    * \brief Equal slope angles.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param n Number of angles, must be even.
    * \param s number of extra angles, must be multiple of 4.
    *
    * This function computes the equal slope angle sequence, modulo \f$\pi\f$, in increasing order and in the interval \f$( \pi / 4, 5\pi / 4 ]\f$, plus angles determined by the extra rays.
    */
   template< class d_type = double >
   std::vector< d_type > equal_slope_angles( int n, int s = 0 )
   {
      // Vector for storing the angles:
      std::vector< d_type > retval;
      retval.reserve( n + s );

      // Compute sequence of angles:
      double del_slope = 4.0 / n;
      double curr_slope = 1.0 - ( 1.0 - 0.25 * ( n % 4 ) - ( s / 4 ) ) * del_slope;

      // BV part:
      for( int i = 0; i < ( n + s ) / 2; ++i )
      {
         retval.push_back( std::atan2( 1.0, curr_slope ) );
         curr_slope -= del_slope;
      }

      // BH part:
      for( int i = 0; i < ( n + s ) / 2; ++i )
         retval.push_back( retval[ i ] + HPI );

      return retval;
   }

   /**
    * Compute pseudo-polar grid sample positions at arbitrary angles.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param M Number of samples on each pseudo-polar ray.
    * \param angles Angles at which compute the samples. The angles should be, up to numerical accuracy, in the range \f$( \pi / 4, 5\pi / 4 ]\f$.
    * \param n_angles Number of angles.
    * \param xi Horizontal coordinates of samples.
    * \param upsilon Vertical coordinates of samples.
    */
   template < class d_type >
   void any_angle_pp_grid(
                           int M,
                           d_type const * angles,
                           int n_angles,
                           d_type * xi,
                           d_type * upsilon,
                           d_type sigma_h = 0.0,
                           d_type sigma_v = 0.0
                         )
   {
      for ( int j = 0; j < n_angles; ++j )
      {
         d_type c( std::cos( angles[ j ] ) );
         d_type s( std::sin( angles[ j ] ) );

         d_type del = 2.0 * PI / M;
         d_type start_point = PI - 0.5 * ( M % 2 ) * del;

         // BV angles:
         if ( ( std::abs( s ) > std::abs( c ) ) && ( angles[ j ] <= TQPI ) )
         {
            d_type cotg = c / s;
            for ( int i = 0; i < M; ++i )
            {
               upsilon[ j + i * n_angles ] = start_point - del * i - sigma_v;
               xi[ j + i * n_angles ] = upsilon[ j + i * n_angles ] * cotg;
            }
         }
         // BH angles:
         else
         {
            d_type tg = s / c;
            for ( int i = 0; i < M; ++i )
            {
               xi[ j + i * n_angles ] = -start_point + del * i + sigma_h;
               upsilon[ j + i * n_angles ] = xi[ j + i * n_angles ] * tg;
            }
         }
      }
   }

   /**
    * Compute pseudo-polar grid sample positions at arbitrary angles.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param M Number of samples on each pseudo-polar ray.
    * \param angles Angles at which compute the samples.
    * \param xi Horizontal coordinates of samples.
    * \param upsilon Vertical coordinates of samples.
    */
   template < class d_type >
   void any_angle_pp_grid(
                           int M,
                           std::vector< d_type > const angles,
                           d_type * xi,
                           d_type * upsilon,
                           d_type sigma_h = 0.0,
                           d_type sigma_v = 0.0
                         )
   {
      any_angle_pp_grid< d_type >( M, &(angles[ 0 ]), angles.size(), xi, upsilon, sigma_h, sigma_v );
   }

   /**
    * Compute golden-angle pseudo-polar grid sample positions.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param M Number of samples on each golden-angle pseudo-polar ray.
    * \param N Number of golden-angle pseudo-polar rays.
    * \param xi Horizontal coordinates of samples
    * \param upsilon Vertical coordinates of samples
    *
    */
   template < class d_type >
   void gapp_grid(
                  int M, int N,
                  d_type * xi,
                  d_type * upsilon,
                  d_type sigma_h = 0.0,
                  d_type sigma_v = 0.0
                 )
   {
      any_angle_pp_grid( M, golden_angles< d_type >( N ), xi, upsilon, sigma_h, sigma_v );
   }

      /**
    * Compute pseudo-polar grid sample positions with extra rays.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param M Number of samples on each pseudo-polar ray.
    * \param N Number of pseudo-polar rays.
    * \param S Number of extra rays.
    * \param xi Horizontal coordinates of samples
    * \param upsilon Vertical coordinates of samples
    *
    */
   template < class d_type >
   void pp_grid(
                  int M, int N, int S,
                  d_type * xi,
                  d_type * upsilon,
                  d_type sigma_h = 0.0,
                  d_type sigma_v = 0.0
               )
   {
      int first_I = -M / 2;
      int last_I = ( M % 2 ) ? M / 2 : M / 2 - 1;
      int del_I = -first_I;

      // BH part:
      int first_J = ( ( N + S ) % 4 ) ? -( N + S ) / 4 : -( N + S ) / 4 + 1;
      int last_J = ( N + S ) / 4;
      int del_J = ( N + S ) / 2 - first_J;

      for ( int I = first_I; I <= last_I; ++I )
      {
         double curr_xi = DPI * I / M + sigma_h;
         for ( int J = first_J; J <= last_J; ++J )
         {
            xi[ ( J + del_J ) + ( I + del_I ) * ( N + S ) ] = curr_xi;
            upsilon[ ( J + del_J ) + ( I + del_I ) * ( N + S ) ] = 4.0 * curr_xi * J / N;
         }
      }

      // BV part:
      first_I = ( M % 2 ) ? -M / 2 : -M / 2 + 1;
      last_I = M / 2;
      del_I = -first_I;

      first_J = ( ( N + S ) % 4 ) ? ( N + S ) / 4 : ( N + S ) / 4 - 1;
      last_J = -( N + S ) / 4;

      for ( int I = first_I; I <= last_I; ++I )
      {
         double curr_upsilon = DPI * I / M - sigma_v;
         for ( int J = first_J; J >= last_J; --J )
         {
            upsilon[ ( first_J - J ) + ( M - 1 - I - del_I ) * ( N + S ) ] = curr_upsilon;
            xi[ ( first_J - J ) + ( M - 1 - I - del_I ) * ( N + S ) ] = 4.0 * curr_upsilon * J / N;
         }
      }
   }

   /**
    * Compute pseudo-polar grid sample positions.
    *
    * \tparam d_type Type of numerical data, can be any floating point type.
    * \param M Number of samples on each pseudo-polar ray.
    * \param N Number of pseudo-polar rays.
    * \param xi Horizontal coordinates of samples
    * \param upsilon Vertical coordinates of samples
    *
    */
   template < class d_type >
   void pp_grid(
                  int M, int N,
                  d_type * xi,
                  d_type * upsilon,
                  d_type sigma_h = 0.0,
                  d_type sigma_v = 0.0
               )
   {
      pp_grid( M, N, 0, xi, upsilon, sigma_h, sigma_h );
   }

} // namespace dft

#endif // #ifndef DFT_H
