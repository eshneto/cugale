#ifndef CUGALE_KERNELS_H
#define CUGALE_KERNELS_H

#include "cuKB.h"
#include <complex>
#include "utils.h"
#include <vector>
#include "dft.h"
#include "kaiser_bessel.h"
#include "my_complex.h"

#ifdef __CUDA_ARCH__
#if __CUDA_ARCH__ < 600
// Code taken from CUDA-C programming guide:
__device__ double atomicAdd( double * address, double val )
{
   unsigned long long int * address_as_ull = ( unsigned long long int * ) address;
   unsigned long long int old = * address_as_ull;
   unsigned long long int assumed;
   do {
      assumed = old;
      old = atomicCAS( address_as_ull, assumed,
                      __double_as_longlong( val + __longlong_as_double( assumed ) )
                     );
      // Note: uses integer comparison to avoid hang in case of NaN ( since NaN != NaN )
   } while ( assumed != old );

   return __longlong_as_double( old );
}
#endif // #if __CUDA_ARCH__ < 600
#endif // #ifdef __CUDA_ARCH__

namespace utils {

   // A call to this kernel should use a grid of M blocks. Each block will process
   // a line of at most 1024 elements. The blocks should be linear and code must be called
   // repeated times if m > 1024. Repeated call is responsibility of host for performance
   // reasons because it is best to branch in host code than branching later in device code.
   template < class data_t >
   __global__ void shift_factors_adjust( std::complex< data_t > * shift_factors,
                                         int M, int m, int N, int S,
                                         data_t R_1,
                                         data_t sigma,
                                         data_t epsilon,
                                         int i_0
                                       )
   {
      // Current column:
      int j = threadIdx.x + i_0;

      // Compute constants (I = blockIdx.x, j = threaIdx.x + i_0):
      int I = blockIdx.x;
      data_t alpha = alpha_I( I, M, R_1, sigma );
      data_t varpi = pi< data_t >::value * ( m - 1 ) * alpha / N;
      data_t tau = pi< data_t >::value + epsilon * ( pi< data_t >::value - cukb::cuda_math< data_t >::abs( varpi ) );
      data_t t = static_cast< data_t >( 2.0 ) * pi< data_t >::value * j / N * alpha;
      data_t w = cukb::kaiser_bessel< data_t >( S * tau, tau, t - varpi );

      // Adjust coefficient:
      typename cufft_type< data_t >::complex_type & sf_ref = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( shift_factors )[ j + I * m ];
      sf_ref = { sf_ref.x / w, sf_ref.y / w };
   }

   template < class data_t >
   __global__ void vertical_shift_factors_adjust( std::complex< data_t > * shift_factors,
                                                  int M, int m, int N, int S,
                                                  data_t R_1,
                                                  data_t sigma,
                                                  data_t epsilon,
                                                  int i_0
                                                )
   {
      // Current column:
      int j = threadIdx.x + i_0;

      // Compute constants (I = blockIdx.x, j = threaIdx.x + i_0):
      int I = blockIdx.x;
      data_t alpha = alpha_I( I, M, R_1, sigma );
      data_t varpi = pi< data_t >::value * ( m - 1 ) * alpha / N;
      data_t tau = pi< data_t >::value + epsilon * ( pi< data_t >::value - cukb::cuda_math< data_t >::abs( varpi ) );
      data_t t = static_cast< data_t >( 2.0 ) * pi< data_t >::value * j / N * alpha;
      data_t w = cukb::kaiser_bessel< data_t >( S * tau, tau, t - varpi );

      // Adjust coefficient:
      typename cufft_type< data_t >::complex_type & sf_ref = reinterpret_cast< typename cufft_type< data_t >::complex_type * >( shift_factors )[ j + I * m ];
      sf_ref = { sf_ref.x / w, sf_ref.y / w };
   }

   template < class data_t >
   data_t alpha( int S, data_t tau )
   {
      return S * tau;
   }

//    template < class data_t >
   void create_linear_combinations( int M, int N, int S, int HK, int m, int n,
                                    double const * angles, int n_angles,
                                    int BH_N_shift, int BV_N_shift,
                                    int BH_M_shift, int BV_M_shift,
                                    std::vector< std::complex< double > > & linear_coefficients,
                                    std::vector< std::vector< int > > & combination_indices,
                                    double sigma_h, double sigma_v,
                                    double epsilon
                                  )
   {
      combination_indices.reserve( n_angles );
      int coef_skip = 2 * HK + 1;
      int N_coefs = coef_skip * n_angles;

      for ( unsigned i = 0; i < linear_coefficients.size(); ++i )
         linear_coefficients[ i ] = 0.0;

      for ( int j = 0; j < n_angles; ++j )
      {
         double c = std::cos( double( angles[ j ] ) );
         double s = std::sin( double( angles[ j ] ) );

         // BV angles:
         if ( ( std::abs( s ) > std::abs( c ) ) && ( angles[ j ] <= dft::TQPI ) )
         {
            double eta = ( c / s ) * N / 4.0;
            int del = ( S / 4 ) + ( N / 2 ) - 1 - BV_N_shift;

            int J_eta = std::ceil( eta );

            std::vector< int > curr_indices;
            curr_indices.reserve( 2 * HK + 1 );

            for ( int i = 0; i < M; ++i )
            {
               double varpi = 2.0 * ( n - 1 ) / N * ( 2.0 * dft::PI * ( M - 1 - BV_M_shift - i ) / M - sigma_v );
               double tau_I = dft::PI + epsilon * ( dft::PI - std::abs( varpi ) );
               kb::kb< double > W_object_I( alpha( HK, tau_I ), tau_I );
               #ifdef GPPFFT_DONT_IGNORE_DC
               int I = M - 1 - i - BV_M_shift;
               #endif

               std::complex< double > exp_factor;

               int is_diff = ( J_eta != eta );
               if ( is_diff && !i )
               {
                  curr_indices.push_back( del - ( J_eta + HK ) );
               }

               for ( int k = HK - is_diff; k > 0; --k )
               {
                  exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta + k ) ) * varpi ) );
                  #ifdef GPPFFT_DONT_IGNORE_DC
                  if ( I )
                     #endif
                     linear_coefficients[ coef_skip * j + i * N_coefs + HK - k ] = exp_factor * W_object_I.fourier( eta - ( J_eta + k ) ) / dft::DPI;
                  if ( !i )
                     curr_indices.push_back( del - ( J_eta + k ) );
               }

               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - J_eta ) * varpi ) );
               #ifdef GPPFFT_DONT_IGNORE_DC
               linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = I ? exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI : 1.0;
               #else
               linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI;
               #endif
               if ( !i )
                  curr_indices.push_back( del - J_eta );

               for ( int k = 1; k <= HK; ++k )
               {
                  exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta - k ) ) * varpi ) );
                  #ifdef GPPFFT_DONT_IGNORE_DC
                  if ( I )
                     #endif
                     linear_coefficients[ coef_skip * j + i * N_coefs + HK + k ] = exp_factor * W_object_I.fourier( eta - ( J_eta - k ) ) / dft::DPI;
                  if ( !i )
                     curr_indices.push_back( del - ( J_eta - k ) );
               }
            }
            combination_indices.push_back( curr_indices );
         }
         // BH angles:
         else
         {
            double eta = ( s / c ) * N / 4.0;
            int del = ( S + N ) / 2 + ( S / 4 ) + BH_N_shift;

            int J_eta = std::floor( eta );

            std::vector< int > curr_indices;
            curr_indices.reserve( 2 * HK + 1 );

            for ( int i = 0; i < M; ++i )
            {
               double varpi = 2.0 * ( m - 1 ) / N * ( 2.0 * dft::PI * ( i - BH_M_shift ) / M + sigma_h );
               double tau_I = dft::PI + epsilon * ( dft::PI - std::abs( varpi ) );
               kb::kb< double > W_object_I( alpha( HK, tau_I ), tau_I );
               #ifdef GPPFFT_DONT_IGNORE_DC
               int I = i - BH_M_shift;
               #endif

               std::complex< double > exp_factor;

               int is_diff = ( J_eta != eta );
               if ( is_diff && !i )
               {
                  curr_indices.push_back( del + J_eta - HK );
               }

               for ( int k = HK - is_diff; k > 0; --k )
               {
                  exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta - k ) ) * varpi ) );
                  #ifdef GPPFFT_DONT_IGNORE_DC
                  if ( I )
                     #endif
                     linear_coefficients[ coef_skip * j + i * N_coefs + HK - k ] = exp_factor * W_object_I.fourier( eta - ( J_eta - k ) ) / dft::DPI;
                  if ( !i )
                     curr_indices.push_back( del + ( J_eta - k ) );
               }

               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - J_eta ) * varpi ) );
               #ifdef GPPFFT_DONT_IGNORE_DC
               linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = I ? exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI : 1.0;
               #else
               linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI;
               #endif
               if ( !i )
                  curr_indices.push_back( del + J_eta );

               for ( int k = 1; k <= HK; ++k )
               {
                  exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta + k ) ) * varpi ) );
                  #ifdef GPPFFT_DONT_IGNORE_DC
                  if ( I )
                     #endif
                     linear_coefficients[ coef_skip * j + i * N_coefs + HK + k ] = exp_factor * W_object_I.fourier( eta - ( J_eta + k ) ) / dft::DPI;
                  if ( !i )
                     curr_indices.push_back( del + ( J_eta + k ) );
               }
            }
            combination_indices.push_back( curr_indices );
         }
      }
   }

   template < class data_t >
   void __global__ truncated_sum( int weight_skip, int M, int N, int P,
                                  int * indices,
                                  std::complex< data_t > * weights,
                                  std::complex< data_t > * data,
                                  std::complex< data_t > * result,
                                  int N_0
                                )
   {
      // threadIdx.x + N_0 determines ray, while blockIdx.x determines point in ray.

      // Compute start range of weighted linogram rays:
      int start_ray = indices[ threadIdx.x + N_0 ];

      // Compute position of starting data in weighted linogram:
      int start_lino = blockIdx.x * P + blockIdx.y * ( P * M ) + start_ray;
      int end_lino = start_lino + weight_skip;

      // Compute position of first weight;
      int start_weight = blockIdx.x * ( weight_skip * N ) + ( threadIdx.x + N_0 ) * weight_skip;

      // Temporary variable for resulting computation:
      my_complex< data_t > tmp_out;
      tmp_out.x = tmp_out.y = data_t( 0.0 );

      for ( int i_lino = start_lino; i_lino < end_lino; ++i_lino )
      {
         // Is weight loading going to be coalesced?
         // If not, weight storage layout can be changed so it is going to be.
         // It should be simple to be done, by alternating weight positions instead of
         // storing them consecutively. Perhaps this won't be necessary if caching is
         // smart enough. Experimentation will tell.

         // Compute reference to currently used weight:
         my_complex< data_t > const & curr_weight = ref_complex( weights[ start_weight + ( i_lino - start_lino ) ] );
         // Create reference to currently used linogram element (this is not going to be coalesced):
         my_complex< data_t > const & curr_lino = ref_complex( data[ i_lino ] );

         // Add current parcel:
         tmp_out.x += curr_weight.x * curr_lino.x - curr_weight.y * curr_lino.y;
         tmp_out.y += curr_weight.y * curr_lino.x + curr_weight.x * curr_lino.y;
      }

      // Compute reference of output. Each thread will compute one output position:
      // Access to output is going to be coalesced.
      my_complex< data_t > & out = ref_complex( result[ blockIdx.x * N + blockIdx.y * ( N * M ) + threadIdx.x + N_0 ] );
      out = tmp_out;
   }

   template < class data_t >
   void __global__ adjoint_truncated_sum( int weight_skip, int M, int N, int P,
                                          int * indices,
                                          std::complex< data_t > * weights,
                                          std::complex< data_t > * data,
                                          std::complex< data_t > * result,
                                          int N_0
                                        )
   {
      // threadIdx.x + N_0 determines ray, while blockIdx.x determines point in ray.

      // Compute start range of weighted linogram rays:
      int start_ray = indices[ threadIdx.x + N_0 ];

      // Compute position of starting data in weighted linogram:
      int start_lino = blockIdx.x * P + blockIdx.y * ( P * M ) + start_ray;
      int end_lino = start_lino + weight_skip;

      // Compute position of first weight;
      int start_weight = blockIdx.x * ( weight_skip * N ) + ( threadIdx.x + N_0 ) * weight_skip;

      // Temporary variable for resulting computation:
      my_complex< data_t > tmp = reinterpret_cast< my_complex< data_t > * >( data )[ blockIdx.x * N + blockIdx.y * ( N * M ) + threadIdx.x + N_0 ];

      for ( int i_lino = start_lino; i_lino < end_lino; ++i_lino )
      {
         // Is weight loading going to be coalesced?
         // If not, weight storage layout can be changed so it is going to be.
         // It should be simple to be done, by alternating weight positions instead of
         // storing them consecutively. Perhaps this won't be necessary if caching is
         // smart enough. Experimentation will tell.

         // Compute reference to currently used weight:
         my_complex< data_t > const & curr_weight = ref_complex( weights[ start_weight + ( i_lino - start_lino ) ] );
         // Create reference to currently used linogram element (this is not going to be coalesced):
         data_t * curr_lino_real = reinterpret_cast< data_t * >( result + i_lino );

         // Add current parcel:
         atomicAdd( curr_lino_real, curr_weight.x * tmp.x + curr_weight.y * tmp.y );
         atomicAdd( curr_lino_real + 1, curr_weight.x * tmp.y - curr_weight.y * tmp.x );
      }
   }
} // namespace utils

#endif // #ifndef CUGALE_KERNELS_H
