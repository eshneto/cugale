#ifndef MY_COMPLEX_H
#define MY_COMPLEX_H

namespace utils {

   template< class data_t >
   struct my_complex {
      typedef data_t data_type;
      data_t x;
      data_t y;
   };

   template < class complex_t >
   __device__ my_complex< typename complex_t::value_type > & ref_complex( complex_t & x )
   {
      return reinterpret_cast< my_complex< typename complex_t::value_type > (&) >( x );
   }

   template < class complex_t >
   __device__ my_complex< typename complex_t::value_type > const & ref_complex( complex_t const & x )
   {
      return reinterpret_cast< my_complex< typename complex_t::value_type > const (&) >( x );
   }

} // namespace utils

#endif // #ifndef MY_COMPLEX_H
