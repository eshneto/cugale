// The following 5 lines are required because of a bug preventing boost to be used with
// CUDA 9.0. Newer versions of boost seem not to have this issue.
#ifdef __CUDACC_VER__
#undef __CUDACC_VER__
#endif
#define __CUDACC_VER__ 90000

#include "ccuGALE.h"
#include "cuGALE.h"
#include <complex>

void * cuGALE_create( int m,
                      int n,
                      int b,
                      int M,
                      int N,
                      double const * angles,
                      int S,
                      int N_L,
                      double sigma_h,
                      double sigma_v,
                      double epsilon
                    )
{
   cuGALE< float > * obj = new cuGALE< float >( m, n, b, M, N, angles, S, N_L, sigma_h, sigma_v, epsilon );

   return reinterpret_cast< void * >( obj );
}

void cuGALE_destroy( void * obj )
{
   delete reinterpret_cast< cuGALE< float > * >( obj );
}

void cuGALE_forward( void * obj, void * in, void * out, void * buffer )
{
   reinterpret_cast< cuGALE< float > * >( obj )->forward(
      reinterpret_cast< std::complex< float > * >( in ),
      reinterpret_cast< std::complex< float > * >( out ),
      reinterpret_cast< std::complex< float > * >( buffer )
   );
}

void cuGALE_adjoint( void * obj, void * in, void * out, void * buffer )
{
   reinterpret_cast< cuGALE< float > * >( obj )->adjoint(
      reinterpret_cast< std::complex< float > * >( in ),
      reinterpret_cast< std::complex< float > * >( out ),
      reinterpret_cast< std::complex< float > * >( buffer )
   );
}
