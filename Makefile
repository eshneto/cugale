.PHONY : test_cuGALE
test_cuGALE : test_cuGALE_dir/cuGALE_test
	ln -sf $(shell pwd)/src/python/* test_cuGALE_dir
	cd test_cuGALE_dir && time ./cuGALE_test
	cd test_cuGALE_dir && python -B accuracy_check.py

test_cuGALE_dir/cuGALE_test : src/test_code/cuGALE_test.cu
	rm -rf $(@D)
	mkdir -p $(@D)
	ln -s $(shell pwd)/src/c/* $(@D)
	ln -s $(shell pwd)/src/test_code/* $(@D)
	cd $(@D) && nvcc -O3 -std=c++11 -ccbin g++-5 -rdc=true -m64 -o $(@F) $(<F) -lcufft_static -lculibos -lfftw3 -lfftw3_threads

pycuGALE_dir/ccuGALE.so : $(shell pwd)/src/c/ccuGALE.cu $(shell pwd)/src/c/ccuGALE.h
# 	rm -rf $(@D)
	mkdir -p $(@D)
	ln -sf $(shell pwd)/src/c/* $(@D)
	cd $(@D) && nvcc -O3 -std=c++11 -ccbin g++-5 -rdc=true -m64 --shared --compiler-options "-fPIC" $< -o $(@F) -lcufft_static -lculibos
	cd $(@D) && python -B ../src/python/cuGALE.py

# Notes: I have learned a couple of things about the compilation process.
# 1) -rdc=true seems to be required for some reason (cufft ?)
#    otherwise some weird bugs happen.
# 2) It is automatically activated by the flag -dc which I was using
#    before when doing two-step compilation for the test executable.

.PHONY : clean
clean :
	rm -rf test_cuGALE_dir
	rm -rf pycuGALE_dir
